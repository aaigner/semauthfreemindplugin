# README #

Dieses Plugin für das Mindmapping Tool "Freemind" ermöglicht eine komfortable Verwendung der SemAuth-Anwendung (http://fhd.semauth.de/pub/) 
innerhalb der Freemind-Anwendung. Mit dem Plugin können Webseiten unter Hilfe der SemAuth-Anwendung lokal generiert werden, oder via FTP veröffentlicht 
werden. Zudem sind Hilfe-Optionen hinterlegt, sowie eine Vorschaufunktion integriert.

### About ###
* Version 1.0
* Plugin erstellt am 2016-06-07
* Lizenz GPLv3

### Abhängigkeiten ###
* keine externen Abhänigkeiten werden benötigt
* alle benötigten Anwendungen liegem dem Release bei

### Source Code ###
* der gesamte Quellcode des Plugins ist hier im Repository verfügbar und kann heruntergeladen werden
* Änderungen können vorgenommen werden
* zum Melden von Fehlern bitte das Ticketing-System von bitbucket verwenden

### Installation ###
* die Zip-Datei unter Downloads herunterladen (https://bitbucket.org/aaigner/semauthfreemindplugin/downloads/semauth.zip)
* die Zip-Datei beinhaltet einen Ordner "semauth", sowie eine XML-Datei SemAuth.xml
* die Datei in den Plugin-Ordner der Freemind-Anwendung extrahieren (C:\Programme\Freemind\Plugins\)
* in diesem Ordner sollte nun der Ordner semauth vorhanden sein, sowie die XML-Datei liegen
(C:\Programme\Freemind\Plugins\semauth\*)
(C:\Programme\Freemind\Plugins\SemAuth.xml)
* Freemind MIT ADMINISTRATOR-RECHTEN neu starten (Rechtsklick > Ausführen als Administrator)
* Plugin verwenden

### Funktionen ###
* Hilfe-Menü: 
Enthält Links zur Projektwebseite und Hilfedokumenten
* Export: 
Ermöglicht eine lokale Übersetzung der Mindmap in eine Webseite, sowie einen FTP-Upload der Dateien. Die lokal generierten Webseiten werden im Plugin Ordner im Unterverzeichnis /www/* abgelegt, z. B. (C:\Programme\Freemind\Plugins\semauth\www\NeueMindmap)
* Preview: 
Zeigt den aktuellen Stand der Mindmap als übersetzte Webseite an (in zweitem Fenster)