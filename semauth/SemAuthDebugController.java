package plugins.semauth;

import freemind.extensions.ExportHook;

public class SemAuthDebugController extends ExportHook
{
 	public void startupMapHook() 
 	{		 				 	
 		this.getController().getFrame().setWaitingCursor(false);
 		new SemAuthDebugView(getController());
 	}
}


