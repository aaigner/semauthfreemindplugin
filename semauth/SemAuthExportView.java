package plugins.semauth;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import freemind.modes.ModeController;

public class SemAuthExportView extends JFrame implements ActionListener
{
	private static final long serialVersionUID = 1L;
	private JRadioButton ui_RadioModeLocalDeploy, ui_RadioModeFTPDeploy;
	private JButton ui_ButtonStart, ui_ButtonEnd;
	private JLabel ui_LabelMode, ui_LabelFTPServer, ui_LabelFTPPassword, ui_LabelFTPUsername, ui_LabelFTPPath;
	private JTextField ui_BoxFTPServer, ui_BoxFTPPassword, ui_BoxFTPUsername, ui_BoxFTPPath;
	private ModeController controller;
	
	public SemAuthExportView(ModeController pModeController) 
	{
		// frame configuration
		this.controller = pModeController;
		this.setTitle(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("APP_TITLE") + " " + 
				SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("APP_TITLE_EXPORT"));
		this.setResizable(false);
		this.setLayout(new GridBagLayout());
		
		// set up buttons
		this.ui_ButtonEnd = new JButton(SemAuthProperties.TEXT.get(
				SemAuthLib.getUserLanguage()).get("BUTTON_CLOSE"));
		this.ui_ButtonStart = new JButton(SemAuthProperties.TEXT.get(
				SemAuthLib.getUserLanguage()).get("BUTTON_START_EXPORT"));
		this.ui_ButtonEnd.addActionListener(this);
		this.ui_ButtonStart.addActionListener(this);
				
		// set up radio buttons
		this.ui_RadioModeLocalDeploy = new JRadioButton(SemAuthProperties.TEXT.get(
				SemAuthLib.getUserLanguage()).get("MESSAGE_LOCAL_DEPLOYMENT"));
		this.ui_RadioModeFTPDeploy = new JRadioButton(SemAuthProperties.TEXT.get(
				SemAuthLib.getUserLanguage()).get("MESSAGE_FTP_DEPLOYMENT"));
		this.ui_RadioModeFTPDeploy.addActionListener(this);
		this.ui_RadioModeLocalDeploy.addActionListener(this);
		ButtonGroup bg = new ButtonGroup();
		bg.add(this.ui_RadioModeLocalDeploy);
		bg.add(this.ui_RadioModeFTPDeploy);
		this.ui_RadioModeLocalDeploy.setSelected(true);
		
		// set up labels
		this.ui_LabelMode = new JLabel(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("MESSAGE_CHOOSE_TYPE"));
		this.ui_LabelFTPServer = new JLabel(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("MESSAGE_FTP_HOST"));
		this.ui_LabelFTPUsername = new JLabel(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("MESSAGE_FTP_USER"));
		this.ui_LabelFTPPassword = new JLabel(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("MESSAGE_FTP_PASS"));
		this.ui_LabelFTPPath= new JLabel(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("MESSAGE_FTP_DIR"));
		
		// set up text boxes
		this.ui_BoxFTPServer = new JTextField();
		this.ui_BoxFTPUsername = new JTextField();
		this.ui_BoxFTPPath = new JTextField();
		this.ui_BoxFTPPassword = new JPasswordField();
		
		// fill with credentials
		this.ui_BoxFTPServer.setText(SemAuthProperties.FTP_HOST);
		this.ui_BoxFTPUsername.setText(SemAuthProperties.FTP_USER);
		this.ui_BoxFTPPassword.setText(SemAuthProperties.FTP_PASS);
		this.ui_BoxFTPPath.setText(SemAuthProperties.FTP_DIR);
		
		// add components to frame
		this.add(this.ui_LabelMode, SemAuthLib.makeLayout(1, 1, 3, 1));		
		this.add(this.ui_RadioModeLocalDeploy, SemAuthLib.makeLayout(1, 2, 1, 1));
		this.add(this.ui_RadioModeFTPDeploy, SemAuthLib.makeLayout(2, 2, 1, 1));
		this.add(this.ui_LabelFTPServer, SemAuthLib.makeLayout(1, 3, 1, 1));
		this.add(this.ui_BoxFTPServer, SemAuthLib.makeLayout(2, 3, 2, 1));
		this.add(this.ui_LabelFTPUsername, SemAuthLib.makeLayout(1, 4, 1, 1));
		this.add(this.ui_BoxFTPUsername, SemAuthLib.makeLayout(2, 4, 2, 1));
		this.add(this.ui_LabelFTPPassword, SemAuthLib.makeLayout(1, 5, 1, 1));
		this.add(this.ui_BoxFTPPassword, SemAuthLib.makeLayout(2, 5, 2, 1));
		this.add(this.ui_LabelFTPPath, SemAuthLib.makeLayout(1, 6, 1, 1));
		this.add(this.ui_BoxFTPPath, SemAuthLib.makeLayout(2, 6, 2, 1));
		this.add(this.ui_ButtonStart, SemAuthLib.makeLayout(2, 9, 1, 1));
		this.add(this.ui_ButtonEnd, SemAuthLib.makeLayout(3, 9, 1, 1));
		
		// prepare frame and show it
		this.pack();
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		if(arg0.getSource().equals(this.ui_ButtonEnd))
		{
			this.setVisible(false);
		}
		else if(arg0.getSource().equals(this.ui_ButtonStart))
		{			
			this.controller.getController().getFrame().setWaitingCursor(true);

			if(this.ui_RadioModeFTPDeploy.isSelected())
			{
				String message = SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("ERROR_EMPTY");
				
				if((!this.ui_BoxFTPServer.getText().isEmpty()
						&& !this.ui_BoxFTPUsername.getText().isEmpty()
						&& !this.ui_BoxFTPPassword.getText().isEmpty()))
				{
					if(SemAuthLib.ftpDeployment(this.controller, 
							this.ui_BoxFTPServer.getText(), 
							this.ui_BoxFTPUsername.getText(), 
							this.ui_BoxFTPPassword.getText(), 
							this.ui_BoxFTPPath.getText()))
					{
						message = SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("MESSAGE_EXPORT_OK");
					}
					else
					{
						message = SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("ERROR_INTERNAL");
					}
				}
				
				JOptionPane.showMessageDialog(this, message);
			}
			else
			{
				String message = SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("ERROR_EMPTY");
				
				if(SemAuthLib.localDeployment(this.controller))
				{
					message = SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("MESSAGE_EXPORT_OK");
				}
				else
				{
					message = SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("ERROR_INTERNAL");
				}				
				
				JOptionPane.showMessageDialog(this, message);
				this.controller.getController().getFrame().setWaitingCursor(false);
			}
		}
	}
}
