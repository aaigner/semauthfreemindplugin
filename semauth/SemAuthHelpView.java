package plugins.semauth;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.net.*;
import javax.swing.*;

public class SemAuthHelpView extends JFrame implements ActionListener
{
	private static final long serialVersionUID = 1L;
	private JLabel ui_LabelVersion;
	private JButton ui_ButtonOnlineManual, ui_ButtonLocalManual, ui_ButtonClose;	
	
	public SemAuthHelpView()
	{
		// frame configuration
		this.setTitle(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("APP_TITLE") + " " + 
				SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("APP_TITLE_HELP"));
		this.setResizable(false);
		this.setLayout(new GridBagLayout());	
		
		// set up labels
		this.ui_LabelVersion = new JLabel(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("APP_TITLE") + " " +
				SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("APP_VERSION") + " " +
				SemAuthProperties.VERSION);
		
		// set up buttons
		this.ui_ButtonClose = new JButton(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("BUTTON_CLOSE"));
		this.ui_ButtonLocalManual = new JButton(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("BUTTON_OPEN_MANUAL"));
		this.ui_ButtonOnlineManual = new JButton(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("BUTTON_OPEN_HOMEPAGE"));
		this.ui_ButtonClose.addActionListener(this);
		this.ui_ButtonLocalManual.addActionListener(this);
		this.ui_ButtonOnlineManual.addActionListener(this);
		
		// add components to frame
		this.add(this.ui_LabelVersion, SemAuthLib.makeLayout(1, 1, 1, 1));
		this.add(this.ui_ButtonLocalManual, SemAuthLib.makeLayout(1, 2, 1, 1));
		this.add(this.ui_ButtonOnlineManual, SemAuthLib.makeLayout(1, 3, 1, 1));
		this.add(this.ui_ButtonClose, SemAuthLib.makeLayout(1, 4, 1, 1));
		
		// prepare and show frame
		this.pack();
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource().equals(this.ui_ButtonLocalManual))
		{
			boolean error = false;
			
			if (Desktop.isDesktopSupported()) 
			{
				try
				{
			        Desktop.getDesktop().open(new File(SemAuthProperties.MANUAL));
				} 
				catch (IOException ioe) 
				{
					error = true;
				}
			}			 
			else 
			{
				error = true;
			}
			
			if(error)
			{
				JOptionPane.showConfirmDialog(this, 
						SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("ERROR_MANUAL_NOT_FOUND") + SemAuthProperties.MANUAL);
			}
		}
		else if(e.getSource().equals(this.ui_ButtonOnlineManual))
		{
			boolean error = false;
			
			if (Desktop.isDesktopSupported()) 
			{
				try
				{
			        Desktop.getDesktop().browse(new URI(SemAuthProperties.HOMEPAGE));
				} 
				catch (IOException | URISyntaxException ioe) 
				{
					SemAuthLib.debug(ioe.getMessage());
					error = true;
				}
			}			 
			else 
			{
				error = true;
			}
			
			if(error)
			{
				JOptionPane.showConfirmDialog(this, 
						SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("ERROR_BROWSER_NOT_FOUND") + SemAuthProperties.HOMEPAGE);
			}
		}
		else if(e.getSource().equals(this.ui_ButtonClose))
		{
			this.setVisible(false);
		}
	}
}
