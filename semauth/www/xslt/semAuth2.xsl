<?xml version="1.0" encoding="utf-8"?>

<!-- run this file:
ubuntu@ubuntu-VirtualBox:~/a/l/lib/semAuth2/pub$ cd  ~/a/l/lib/semAuth2/pub;  java -classpath ../xslt/saxon9he.jar  net.sf.saxon.Transform ../mm/semAuth2_doku_de_v01.mm ../xslt/semAuth2.xsl t=../xslt/MaxMustermann.html build="`date`"; cp -r ../images/ ../pub/ ../quiz/ ~/a/public_html/jbusse.de/semAuth2
-->

<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:common="http://exslt.org/common"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    version="2.0">

  <!-- process this template with
       http://sourceforge.net/projects/saxon/files/Saxon-HE/9.6/
       http://sourceforge.net/projects/saxon/files/Saxon-HE/9.6/SaxonHE9-6-0-7J.zip/download

java -classpath ~/path/to/jars/saxon9he.jar\
    net.sf.saxon.Transform\
    yourMindmap.mm \
    semAuth2.xsl
  -->
      
  <!-- semAuthLite.xsl is the template which is called from freemind.
       $pass1000 contains the raw html produced by semAuthLite.xsl,
       $fileArray contains the templated output.
  -->

  <!-- a xslt 1.0 stylesheet; however, saxon can execute it -->
  <xsl:import  href="semAuthLite.xsl"/> 

  <!-- output to path -->
  <xsl:param name="p" />


    <!-- restrict transformation to subtrees starting with @TEXT="$TEXT"  -->
  <xsl:param name="TEXT" />

  <!-- css directory name  -->
  <xsl:param name="css" />

  <xsl:variable name="stamp" select="xs:dateTime('1970-01-01T00:00:00') + xs:dayTimeDuration('PT1450686834.123S')" />
  <xsl:param name="build" select="format-dateTime($stamp, '[Y0001]-[M01]-[D01] T [H01]:[m01]:[s01]')" /> <!-- build information, e.g. date of las run -->
  
  <!-- we generate a well formed html5 file -->
  <xsl:output
      method="html"
      encoding="utf-8"  
      omit-xml-declaration="yes"
      doctype-system="about:legacy-compat"
      indent="yes"
      />

    <xsl:output name="moodleXml"
		method="xml"
		cdata-section-elements="text"
      encoding="utf-8"  
      omit-xml-declaration="no"
      indent="yes"
      />

  
  <xsl:template match="/">
    <!-- nur xslt2 -->
	 <xsl:message>ZEIT: <xsl:value-of
	 select='format-dateTime($stamp, "[Y0001]-[M01]-[D01] T [H01]:[m01]:[s01]" ) '/>; </xsl:message>
   
	 <xsl:apply-templates select="common:node-set($fileArray)" mode="final"  />

	 <xsl:choose>
	   <xsl:when test="common:node-set($fileArray)//quiz">
	     <xsl:message>
	       <xsl:text>Exporting Moodle </xsl:text>
	       <xsl:value-of select="$p"/>../quiz/<xsl:text>quiz.xml (</xsl:text>
	       <xsl:value-of select="count(common:node-set($fileArray)//quiz/question[@type!='category'])"/><xsl:text> questions, </xsl:text>
	       <xsl:value-of select="count(common:node-set($fileArray)//answer)"/><xsl:text> answers, </xsl:text>
	       <xsl:value-of select="count(common:node-set($fileArray)//quiz/question[@type='category'])"/><xsl:text> categories)</xsl:text>
	     </xsl:message>
	     <xsl:result-document  format="moodleXml" href="{$p}../quiz/quiz.xml" >
	       <quiz>
		 <xsl:copy-of select="common:node-set($fileArray)//quiz/node()" />
	       </quiz>
	     </xsl:result-document>
	   </xsl:when>
	 </xsl:choose>

  </xsl:template>


  
  
  <!-- superseeds the resp. template in semAuthLite.xsl -->
  <xsl:template match="div[@type='file']" mode="final">
    <xsl:choose>

      <xsl:when test="system-property('xsl:version') = '2.0' and $multipleFiles = 'yes' ">

	<xsl:variable name="outputFile">
	  <xsl:value-of select="fn:encode-for-uri(@id)"/>
	  <xsl:text>.html</xsl:text>
	</xsl:variable>

	<xsl:variable name="config" select="@config" />
	<xsl:variable name="outputDir" select="common:node-set($pass500)//(node[@tagChar='A']/node[@TEXT=$config]/node[@TEXT='outputDir'])[1]/node/@TEXT" />

	<xsl:message>Writing to <xsl:value-of select="$p"/><xsl:value-of select="$outputDir"/><xsl:value-of select="$outputFile"/></xsl:message>
	<xsl:result-document href="{$p}{$outputDir}{$outputFile}" >
	  <xsl:apply-templates select="node()" mode="final" />
	</xsl:result-document>
      </xsl:when>

      <xsl:otherwise>
	<xsl:copy-of select="node()" />
      </xsl:otherwise>
      
      <!-- <vendor><xsl:value-of select="system-property('xsl:version')" /> by <xsl:value-of select="system-property('xsl:vendor')" /> (<xsl:value-of select="system-property('xsl:vendor-url')" />)</vendor> -->
    </xsl:choose>

  </xsl:template>
  
    
  
</xsl:stylesheet>
