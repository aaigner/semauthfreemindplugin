<?xml version="1.0" encoding="utf-8"?>

  <!-- Erzeugt ein Moodle-XML-Format:
       https://docs.moodle.org/27/de/Moodle_XML-Format
  -->

<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:common="http://exslt.org/common"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xalan="http://xml.apache.org/xalan"
    xmlns:str="http://exslt.org/strings"
    exclude-result-prefixes="xalan common fn str"
    version="1.0">

  <xsl:output
      method="xml"
      encoding="utf-8"  
      omit-xml-declaration="no"
      indent="yes"
      />
  

  <xsl:template match="/">
    <xsl:choose>
      <xsl:when test="/map"> <!-- we have a mindmap as input -->
	<quiz>
	  <xsl:apply-templates select="common:node-set($clean)//node[starts-with(icon/@BUILTIN, 'full-')]" mode="questionTextCategory" />
	</quiz>
      </xsl:when>
      <xsl:when test="/quiz">
	<xsl:apply-templates select="/quiz" mode="quiz2mm"  />
      </xsl:when>
      <xsl:when test="/GLOSSARY">
	<xsl:apply-templates select="//GLOSSARY/INFO" mode="GLOSSARY"  />
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:variable name="clean">
    <xsl:apply-templates select="/" mode="clean" />
  </xsl:variable>
  
  <xsl:template match="*[icon/@BUILTIN='button_cancel'] " mode="clean" />

  <xsl:template match="/ | node() | @*" mode="clean">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="clean" />
    </xsl:copy>
  </xsl:template>



  <!-- copied also to semAuth2 -->
  <xsl:variable name="defaultQuestionFormat">moodle_auto_format</xsl:variable>
    
  <xsl:variable name="fractions">
	  <frac n="1" m="100" />
	  <frac n="2" m="50" />
	  <frac n="3" m="33.33333" />
	  <frac n="4" m="25" />
	  <frac n="5" m="20" />
	  <frac n="6" m="16.66667" />
	  <frac n="7" m="14.28571" />
	  <frac n="8" m="12.5" />
	  <frac n="9" m="11.11111" />
  </xsl:variable>

  <xsl:template match="*" mode="questionTextCategory">
    <xsl:message>ERROR 47:  template should not be selected:  match="*" mode="questionTextCategory" </xsl:message>
  </xsl:template>


  <xsl:template match="node[starts-with(icon/@BUILTIN, 'full-')]" mode="questionTextCategory">
    <xsl:variable name="category">
      <xsl:apply-templates select=".." mode="textCategory" />
    </xsl:variable>
    <xsl:comment>
### CATEGORY <xsl:value-of select="$category"/> ###
    </xsl:comment>
    <question type="category">
      <category>
        <text>
	  <xsl:value-of select="$category"/>
	</text>
      </category>
    </question>
    <xsl:apply-templates select="node" mode="questionText" />
  </xsl:template>


  
  <xsl:template match="*" mode="textCategory" />

  
  <xsl:template match="node" mode="textCategory">
    <xsl:choose>
      <xsl:when test="ancestor::node[2][not(@tagChar or name() = 'map')]">
	<xsl:apply-templates select=".." mode="textCategory" />
	<xsl:text>/</xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:value-of select="@TEXT" />
  </xsl:template>


  <xsl:template name="questionName">

    <name>
      <text>
	<xsl:value-of select="normalize-space(../../@TEXT)" /> <!-- most specific category -->
	<!-- really show mindmapQuestionType? 
	    <xsl:text>_</xsl:text>
	    <xsl:value-of select="normalize-space(../@TEXT)" />
	    -->
	<xsl:if test="position() &gt; 1">  
	  <xsl:text>_</xsl:text>
	  <xsl:value-of select="position()" />
	</xsl:if>
      </text>
    </name>
  </xsl:template>

  <xsl:template name="questionText">
    <questiontext format="{$defaultQuestionFormat}">
      <text>
	<xsl:value-of select="@TEXT" />
	<!--<xsl:value-of select="richcontent[@TYPE='NODE']/html/body/(* | text() ) " />-->
      </text>
    </questiontext>
    <defaultgrade>
      <xsl:value-of select="substring-after(../icon/@BUILTIN, 'full-')" />
    </defaultgrade>

  </xsl:template>
  


  <xsl:template match="node" mode="questionText">
    <xsl:variable name="mindmapQuestionType" select="normalize-space(../@TEXT)" />


    <xsl:choose>

      <!-- MC: one or more buttons can be selected
	   selecting: one of many answers can be selected
      -->
      <xsl:when test="$mindmapQuestionType = 'MC' " >
	<xsl:choose>
	  
	  <!-- multiple choice -->
          <xsl:when test="node[@TEXT='T'] and node[@TEXT='F']
			  and not ( node[@TEXT !='T' and @TEXT != 'F'] ) ">
	    <xsl:variable name="T">
	      <xsl:value-of select="count(node[@TEXT='T']/node)" />
	    </xsl:variable>
	    <xsl:variable name="F">
	      <xsl:value-of select="count(node[@TEXT='F']/node)" />
	    </xsl:variable>

      	    <question type="multichoice">
	      <xsl:call-template name="questionName" /><xsl:call-template name="questionText" />
	      <single>false</single>
	      <shuffleanswers>true</shuffleanswers>
	      <!-- true answers -->
	      <xsl:for-each select="node[@TEXT='T']/node">
		<answer format="{$defaultQuestionFormat}" fraction="{$fractions/frac[@n=$T]/@m}">
		  <text><xsl:value-of select="@TEXT" /></text>
		  <xsl:if test="node">
		    <feedback><text><xsl:value-of select="node/@TEXT" /></text></feedback>
		  </xsl:if>
		</answer>
	      </xsl:for-each>
	      <!-- false answers -->
	      <xsl:for-each select="node[@TEXT='F']/node">
		<answer format="{$defaultQuestionFormat}" fraction="-{$fractions/frac[@n=$F]/@m}">
		  <text><xsl:value-of select="@TEXT" /></text>
		  <xsl:if test="node">
		    <feedback><text><xsl:value-of select="node/@TEXT" /></text></feedback>
		  </xsl:if>
		</answer>
	      </xsl:for-each>
	    </question>
	  </xsl:when>



	  <!-- most generic case: matching question, where the first layer of nodes
	       represents the answers, the next layer the questions  -->
	  <xsl:otherwise>
	    <question type="matching">
	      <xsl:call-template name="questionName" /><xsl:call-template name="questionText" />
	      <shuffleanswers>true</shuffleanswers>
	      <xsl:for-each select="node/node">
		<subquestion format="{$defaultQuestionFormat}">
		  <text><xsl:value-of select="@TEXT" /></text>
		  <answer><text><xsl:value-of select="../@TEXT" /></text></answer>
		</subquestion>
	      </xsl:for-each>
	      <xsl:for-each select="node[not(node)]">
		<subquestion format="{$defaultQuestionFormat}">
		  <text></text>
		  <answer><text><xsl:value-of select="@TEXT" /></text></answer>
		</subquestion>
	      </xsl:for-each>
	      <!-- nötig?
	      <xsl:if test="not(node[not(node)])">
		<subquestion format="{$defaultQuestionFormat}">
		  <text></text>
		  <answer><text>false</text></answer>
		</subquestion>
	      </xsl:if>-->
	    </question>
	  </xsl:otherwise>

	</xsl:choose>
      </xsl:when>

      
      <!-- other questions -->

      <!-- single choice: only one button can be selected -->
      <xsl:when test="$mindmapQuestionType = 'SC'">
	<question type="multichoice">
	  <xsl:call-template name="questionName" /><xsl:call-template name="questionText" />
	  <single>true</single>
	  <shuffleanswers>true</shuffleanswers>
	  <xsl:for-each select="node[@TEXT='T']/node">
	    <answer format="{$defaultQuestionFormat}" fraction="100" >
	      <text><xsl:value-of select="@TEXT" /></text>
	      <xsl:if test="node">
		<feedback><text><xsl:value-of select="node/@TEXT" /></text></feedback>
	      </xsl:if>
	    </answer>
	  </xsl:for-each>

	  <xsl:for-each select="node[@TEXT='F']/node">
	    <answer format="{$defaultQuestionFormat}" fraction="0" >
	      <text><xsl:value-of select="@TEXT" /></text>
	      <xsl:if test="node">
		<feedback><text><xsl:value-of select="node/@TEXT" /></text></feedback>
	      </xsl:if>
	    </answer>
	  </xsl:for-each>

	 <xsl:for-each select="node[not(@TEXT='F' or @TEXT='T')]/node">
	    <xsl:message>   WARNING 252: skipping SC answer text "<xsl:value-of select="../@TEXT" />: <xsl:value-of select="@TEXT" />" </xsl:message>
	  </xsl:for-each>
	  
	</question>
      </xsl:when>


      <!-- true or false -->
      <xsl:when test="$mindmapQuestionType = 'TF'  or $mindmapQuestionType = 'FT'" >
	<question type="truefalse">
	  <xsl:call-template name="questionName" /><xsl:call-template name="questionText" />
	  <xsl:choose>

	    <xsl:when test="$mindmapQuestionType = 'TF'">
	      <answer format="{$defaultQuestionFormat}">
		<text>true</text>
		<feedback><text><xsl:value-of select="node[1]/@TEXT" /></text></feedback>
	      </answer>
	      <answer format="{$defaultQuestionFormat}">
		<text>false</text>
		<feedback><text><xsl:value-of select="node[2]/@TEXT" /></text></feedback>
	      </answer>
	    </xsl:when>

	    <xsl:otherwise>
	      <answer format="{$defaultQuestionFormat}">
		<text>false</text>
		<feedback><text><xsl:value-of select="node[1]/@TEXT" /></text></feedback>
		</answer>
	      <answer format="{$defaultQuestionFormat}">
		<text>true</text>
		<feedback><text><xsl:value-of select="node[2]/@TEXT" /></text></feedback>
	      </answer>
	    </xsl:otherwise>
	    
	  </xsl:choose>
	</question>
      </xsl:when>

      
      <!-- short answer -->
      <xsl:when test="$mindmapQuestionType = 'SA'" >
	<question type="shortanswer">
	  <xsl:call-template name="questionName" /><xsl:call-template name="questionText" />
	  <shuffleanswers>true</shuffleanswers>
	  <xsl:for-each select="node">
	    <answer format="{$defaultQuestionFormat}" fraction="100" >
	      <text><xsl:value-of select="@TEXT" /></text>
	      <xsl:if test="node">
		<feedback><text><xsl:value-of select="node/@TEXT" /></text></feedback>
	      </xsl:if>
	    </answer>
	  </xsl:for-each>
	</question>
      </xsl:when>

      <!-- answer, short -->
      <xsl:when test="$mindmapQuestionType = 'AS'" >
	  <question type="shortanswer">
	    <xsl:call-template name="questionName" /><xsl:call-template name="questionText" />
	    <shuffleanswers>true</shuffleanswers>
	    <xsl:for-each select="node">
	      <answer format="{$defaultQuestionFormat}" fraction="100" >
		<text><xsl:value-of select="@TEXT" /></text>
		<xsl:if test="node">
		  <feedback><text><xsl:value-of select="node/@TEXT" /></text></feedback>
		</xsl:if>
	      </answer>
	    </xsl:for-each>
	</question>
      </xsl:when>

      
      <!-- random short answer match -->
      <xsl:when test="$mindmapQuestionType = 'RSA'" >
	<question type="randomsamatch">
	  <xsl:call-template name="questionName" /><xsl:call-template name="questionText" />
	  <choose>2</choose>
	  <subcats>1</subcats>
	</question>
      </xsl:when>

      
      <!-- random multiple choice answer match -->
      <xsl:when test="$mindmapQuestionType = 'RMC'" >
	<question type="randomsamatch">
	  <xsl:call-template name="questionName" /><xsl:call-template name="questionText" />
	  <choose>2</choose>
	  <subcats>1</subcats>
	</question>

	<!-- generate a lot of single SA -->
	<xsl:for-each select="node/node">
	  <question type="shortanswer">
	    <name>
	      <text>
		<xsl:value-of select="normalize-space(../../../../@TEXT)" /> <!-- most specific category -->
		<xsl:text>_RMC_</xsl:text>
		<xsl:value-of select="generate-id()" />
	      </text>
	    </name>
	    <questiontext format="{$defaultQuestionFormat}">
	      <text>
		<!-- <xsl:value-of select="generate-id()" /> -->
		<xsl:value-of select="@TEXT" />
	      </text>
	    </questiontext>
	    <answer format="{$defaultQuestionFormat}" fraction="100" >
	      <text><xsl:value-of select="../@TEXT" /></text>
	    </answer>
	  </question>
	</xsl:for-each>
      </xsl:when>



      
      <!-- cloze (OU) -->
      <xsl:when test="$mindmapQuestionType = 'CZ'" >
	<question type="gapselect">
	  <xsl:call-template name="questionName" />

	  <!-- questiontext with n links to the correct answers -->
	  <questiontext format="{$defaultQuestionFormat}">
	    <text>

	      <xsl:text>&lt;p&gt;</xsl:text>
	      <xsl:value-of select="@TEXT" />
	      <xsl:text>&lt;/p&gt;</xsl:text>
	      
	      <xsl:text>&lt;p&gt;</xsl:text>

		<xsl:text> </xsl:text>
		<xsl:for-each select="node[node]">

		  <xsl:variable name="myGenId">
		    <xsl:value-of select="generate-id()" />
		  </xsl:variable>
		  
		  <xsl:value-of select="@TEXT" />

		  <xsl:if test="node">
		    <xsl:text> [[</xsl:text>
		    <xsl:value-of select="position()" />
	    	    <xsl:text>]] </xsl:text>
		  </xsl:if>

		  <xsl:value-of select="node//node/@TEXT" />
		  
		  <xsl:text> </xsl:text>

		  <xsl:for-each select="following-sibling::node
					[not(node)]
					[generate-id(preceding-sibling::node[1]) = $myGenId]">
		    <xsl:value-of select="@TEXT" />
		  </xsl:for-each>
		  
		</xsl:for-each>
		<xsl:text>&lt;/p&gt;</xsl:text>


	    </text>
	  </questiontext>

	  <defaultgrade>
	    <xsl:value-of select="count(node[node])" />
	  </defaultgrade>

	  <shuffleanswers>1</shuffleanswers>

	  <!-- generate first n correct answers -->
	  <xsl:comment>first set: the correct answers</xsl:comment>
	  <xsl:for-each select="node[node]">
	    <selectoption>
	      <text><xsl:value-of select="node[1]/@TEXT" /></text>
	      <group>
		<xsl:choose>
		  <xsl:when test="node[2]" >
		    <xsl:value-of select="position()" />
		  </xsl:when>
		  <xsl:otherwise>
		    <xsl:text>1</xsl:text>
		  </xsl:otherwise>
		</xsl:choose>
	      </group>
	    </selectoption>
	  </xsl:for-each>

	  <!-- generate additional n+1 .. m false answers -->
	  <xsl:comment>second set: false answers</xsl:comment>
	  <xsl:for-each select="node[node]">
	    <xsl:variable name="group">
	      <xsl:value-of select="position()" />
	    </xsl:variable>
	    <xsl:for-each select="node">
	      <xsl:if test="position() &gt; 1">
		<selectoption>
		  <text><xsl:value-of select="@TEXT" /></text>
		  <group>
		    <xsl:value-of select="$group" />
		  </group>
		</selectoption>
	      </xsl:if>
	    </xsl:for-each>
	  </xsl:for-each>
	</question>
      </xsl:when>


      <!-- order, de: Anordnen 
      <xsl:when test="$mindmapQuestionType = '123'" >
	<question type="shortanswer">
	  <xsl:call-template name="ordering" />
	  <selecttype>ALL</selecttype>
	  <selectcount>
	    <xsl:value-of select="count(node)" />
	  </selectcount>
	  <xsl:for-each select="node">
	    <answer format="{$defaultQuestionFormat}" fraction="{position()}" >
	      <text><xsl:value-of select="@TEXT" /></text>
	    </answer>
	  </xsl:for-each>
	</question>
      </xsl:when>
      -->

      
      <xsl:otherwise>
	<xsl:message> ERROR 232: Unknown mm question type "<xsl:value-of select="$mindmapQuestionType" />"
	in question "<xsl:value-of select="@TEXT" />" </xsl:message>
      </xsl:otherwise>

      
    </xsl:choose>
      
  </xsl:template>

  <!-- END copy to semAuth2 -->


  


  

  <!-- #### Sonderfunktion: inverse function: mode="quiz2mm" ### -->

  <xsl:template match="/quiz" mode="quiz2mm" >
    <map>
      <node TEXT="quiz2mm">
	<xsl:apply-templates select="question" mode="quiz2mm"  />
      </node>
    </map>
  </xsl:template>

  <xsl:template match="question[type='category']" mode="quiz2mm" >
    <node TEXT="{category/text/text()}">
      <xsl:apply-templates select="question" mode="quiz2mm"  />
    </node>
  </xsl:template>
  

  <xsl:template match="node() | text()" mode="rc">
    <xsl:copy>
      <xsl:apply-templates select="node() | text()"  mode="rc" />
    </xsl:copy>
  </xsl:template>



  
  <!-- #### Sonderfunktion: GLOSSARY einlesen ### -->
    
  <xsl:template match="node() | @*" mode="GLOSSARY" >
    <ERROR><xsl:value-of select="name()" /></ERROR>
    <!--<xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="GLOSSARY" />
    </xsl:copy>-->
  </xsl:template>
  
  <xsl:template match="/GLOSSARY/INFO" mode="GLOSSARY" >
    <map>
      <node TEXT="{NAME}" >
	<node TEXT="generiert mit mm2moodleTest 2015-12-03" >
	  <xsl:apply-templates select="ENTRIES/ENTRY" mode="GLOSSARY" />
	</node>
      </node>
    </map>
  </xsl:template>

  <xsl:template match="ENTRY" mode="GLOSSARY" >
    <node TEXT="MC">
      <icon BUILTIN="full-9" />
      <node TEXT="{CONCEPT}">
	<node TEXT="T">
	  <xsl:apply-templates select="ALIASES/ALIAS" mode="GLOSSARY" />
	</node>
	<node TEXT="?" >
	  <xsl:apply-templates select="DEFINITION/* | DEFINITION/text()" mode="GLOSSARY" />
	</node>
      </node>
    </node>
  </xsl:template>

  <xsl:template match="ALIAS" mode="GLOSSARY" >
    <xsl:choose>
      <xsl:when test="contains(NAME,'|')">
	<node TEXT="{normalize-space(substring-before(NAME, '|'))}">
	  <node TEXT="{normalize-space(substring-after(NAME, '|'))}" />
	</node>
      </xsl:when>
      <xsl:otherwise>
	<node TEXT="{normalize-space(NAME)}" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="DEFINITION/text()" mode="GLOSSARY" >
    <xsl:if test="normalize-space(.) != '' ">
      <node TEXT="text()={.}" />
    </xsl:if>
  </xsl:template>

  <xsl:template match="DEFINITION/p" mode="GLOSSARY" >
    <node TEXT="{.}" />
  </xsl:template>

  <xsl:template match="DEFINITION/ul | DEFINITION/ol" mode="GLOSSARY" >
    <xsl:for-each select="li">
      <node TEXT="{.}" />
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="DEFINITION/*" priority="0.3" mode="GLOSSARY" >
    <node TEXT="{.}" />
  </xsl:template>





  
  
  <xsl:template match="quiz" mode="MMC-XML2html">
      <xsl:apply-templates select="question" mode="MMC-XML2html" />
  </xsl:template>

  <xsl:template match="question[@type='category']" mode="MMC-XML2html" />


  <xsl:template match="question[@type='multichoice' or @type='truefalse']" mode="MMC-XML2html">
    <h3><xsl:value-of select="preceding-sibling::question[@type='category'][1]/category" /> > <xsl:value-of select="name" />:</h3>
    <p><xsl:value-of select="questiontext" /></p>

    <table border="1">
      <tr>
	<th>true</th>
	<th>false</th>
      </tr>
      <tr>
	<td>
	  <ul>
	    <xsl:for-each select="answer[not(starts-with(@fraction,'-')) or text='true']">
	      <li>
		<xsl:value-of select="." />
	      </li>
	    </xsl:for-each>
	  </ul>
	</td>
	<td>
	  <ul>
	    <xsl:for-each select="answer[starts-with(@fraction,'-') or @fraction='0' or text='false' ]">
	      <li>
		<xsl:value-of select="." />
	      </li>
	    </xsl:for-each>
	  </ul>
	</td>
      </tr>
    </table>
  </xsl:template>


  <xsl:template match="question[@type='shortanswer']" mode="MMC-XML2html">
    <h3><xsl:value-of select="preceding-sibling::question[@type='category'][1]/category" /> > <xsl:value-of select="name" />:</h3>
    <p><xsl:value-of select="questiontext" /></p>
	  <ul>
	    <xsl:for-each select="answer">
	      <li>
		<xsl:value-of select="." />
	      </li>
	    </xsl:for-each>
	  </ul>
  </xsl:template>


  

     
  <xsl:template match="question[@type='matching']" mode="MMC-XML2html">
    <h3><xsl:value-of select="preceding-sibling::question[@type='category'][1]/category" /> > <xsl:value-of select="name" />:</h3>
    <p><xsl:value-of select="questiontext" /></p>
    <table border="1">
      <tr>
	<th>question</th>
	<th>select from</th>
      </tr>
      <xsl:for-each select="subquestion">
	<tr>
	  <td>
	    <xsl:value-of select="text" />
	  </td>
	  <td>
	    <xsl:value-of select="answer" />
	  </td>
	</tr>
      </xsl:for-each>
    </table>
    
  </xsl:template>


  


  






  


  
  
</xsl:stylesheet>


