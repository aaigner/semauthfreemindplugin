<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:common="http://exslt.org/common"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xalan="http://xml.apache.org/xalan"
    xmlns:str="http://exslt.org/strings"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:date="http://exslt.org/dates-and-times"
    xmlns:dyn="http://exslt.org/dynamic"
    xmlns:saxon="http://icl.com/saxon"
    extension-element-prefixes="saxon dyn"
    exclude-result-prefixes="xalan common fn str xs date"
    version="1.0">

    <xsl:import href="mm2moodleTest.xsl"/>
    
    <!-- freeplane requires encoding ISO-8859-1 -->
    <!--
	<xsl:output
	method="freeplane"
	encoding="iso-8859-1"  
	omit-xml-declaration="no"
	indent="yes"
	saxon:character-representation="entity;decimal"
	saxon:suppress-indentation="b em i"
	/>
    -->

    <xsl:output
      method="xml"
      encoding="utf-8"  
      omit-xml-declaration="yes"
      indent="yes"
      saxon:character-representation="entity;decimal"
      saxon:suppress-indentation="b em i"
      />

  <xsl:param name="o" select="'fileArray'" /> <!-- output -->


  
  <!-- path to the file name of user template -->
  <xsl:param name="t" select="'../xslt/semAuthTemplate.html'" />
  <xsl:variable name="templateLocation" select="$t" />

  <!-- debug information during transformation, sent to STDERR -->
  <xsl:param name="verbose" select="'2'" /> <!-- output -->

  <!-- manually given build information, e.g. date of las run -->
  <xsl:param name="build" /> 

  <!-- restrict transformation to subtrees starting with @TEXT="$TEXT"  -->
  <xsl:param name="TEXT" />

  <!-- restrict output in late phases to node(s) with @id="$i" -->
  <xsl:param name="i" /> 

  <xsl:param name="m" select="'yes'" />
  <xsl:variable name="multipleFiles">
    <xsl:choose>
      <xsl:when test="$m='yes' and system-property('xsl:version') = '2.0' ">yes</xsl:when>
      <xsl:otherwise>no</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="lTag">
      <l tag="(de)" />
      <l tag="(en)" />
      <l tag="(ru)" />
  </xsl:variable>

  <xsl:variable name="myEncodeForUriPositive">_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789()</xsl:variable>
  
  <xsl:variable name="htmlExt">.html</xsl:variable>

  <xsl:variable name="uriScheme">
    <s>aaa:</s><s>aaas:</s><s>acap:</s><s>cap:</s><s>cid:</s><s>crid:</s><s>data:</s><s>dav:</s><s>dict:</s><s>dns:</s><s>fax:</s><s>file:</s><s>ftp:</s><s>geo:</s><s>go:</s><s>gopher:</s><s>h323:</s><s>http:</s><s>https:</s><s>im:</s><s>imap:</s><s>Info:</s><s>ldap:</s><s>mailto:</s><s>mid:</s><s>news:</s><s>nfs:</s><s>nntp:</s><s>pop:</s><s>pres:</s><s>rtsp:</s><s>sip:</s><s>sips:</s><s>snmp:</s><s>tag:</s><s>tel:</s><s>telnet:</s><s>urn:</s><s>wais:</s><s>xmpp:</s><s>about:</s><s>afp:</s><s>aim:</s><s>apt:</s><s>bolo:</s><s>bzr:</s><s>callto:</s><s>coffee:</s><s>cvs:</s><s>daap:</s><s>ed2k:</s><s>feed:</s><s>fish:</s><s>gg:</s><s>git:</s><s>gizmoproject:</s><s>iax2:</s><s>irc:</s><s>ircs:</s><s>itms:</s><s>ldaps:</s><s>magnet:</s><s>mms:</s><s>msnim:</s><s>postal2:</s><s>rsync:</s><s>secondlife:</s><s>skype:</s><s>spotify:</s><s>ssh:</s><s>svn:</s><s>sftp:</s><s>smb:</s><s>sms:</s><s>steam:</s><s>view-source:</s><s>vzochat:</s><s>webcal:</s><s>winamp:</s><s>wyciwyg:</s><s>xfire:</s><s>ymsgr:</s>
  </xsl:variable>

  <!-- root template -->

  <xsl:template match="/">

    
    <xsl:call-template  name="message">
      <xsl:with-param name="bias" select="'2'"/>
      <xsl:with-param name="theMessage">START; build=<xsl:value-of select="$build" />; #nodes=<xsl:value-of select="count(//node)" /></xsl:with-param>
    </xsl:call-template>
    
    <xsl:choose>

      <!-- misc. intermediate passes for debugging -->
      <xsl:when test="$o = '500'">
	<xsl:copy-of select="common:node-set($pass500)" />
      </xsl:when>

      <xsl:when test="$o = 'nf1'">
	<xsl:copy-of select="common:node-set($nf1)" />
      </xsl:when>

      <xsl:when test="$o = 'richcontent'">
	<xsl:copy-of select="common:node-set($richcontent)" />
      </xsl:when>

      <xsl:when test="$o = 'candidateId'">
	<xsl:copy-of select="common:node-set($candidateId)" />
      </xsl:when>

      <xsl:when test="$o = 'id'">
	<xsl:copy-of select="common:node-set($id)" />
      </xsl:when>

      <!--
	  <xsl:when test="$o = 'languageTags'">
	  <xsl:copy-of select="common:node-set($languageTags)" />
	  </xsl:when>
      -->

      <xsl:when test="$o = 'mmIdRef'">
	<xsl:copy-of select="common:node-set($mmIdRef)" />
      </xsl:when>

      <xsl:when test="$o = 'uriEncode'">
	<xsl:copy-of select="common:node-set($uriEncode)" />
      </xsl:when>
      
      <xsl:when test="$o = 'nf2'">
	<xsl:copy-of select="common:node-set($nf2)" />
      </xsl:when>

      <xsl:when test="$o = 'rawHtml'">
	<xsl:copy-of select="common:node-set($rawHtml)" />
      </xsl:when>

      <xsl:when test="$o = 'fileHref'">
	<xsl:copy-of select="common:node-set($fileHref)" />
      </xsl:when>

      <xsl:when test="$o = 'TOC'">
	<xsl:copy-of select="common:node-set($TOC)" />
      </xsl:when>

      <xsl:when test="$o = '1000'">
	<xsl:copy-of select="common:node-set($pass1000)" />
      </xsl:when>

      <xsl:when test="$o = 'fileArray'">
	<xsl:copy-of select="common:node-set($fileArray)" />
      </xsl:when>

      
      <xsl:otherwise>      
	<xsl:message>unknown value of java parameter o=<xsl:value-of select="$o" />.
	Allowed values are: 500 nf1</xsl:message>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>




  <!-- $$$$$ variables represent a pipeline $$$$$ -->


  <!-- VARIABLES pass0 - pass150 -->
  <!-- throw away needless stuff; delete nodes with X icon; normalize space -->
  
  <xsl:template match="node()" mode="pass0">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="pass0" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="node" priority="1" mode="pass0">
    <xsl:choose>

      <xsl:when test="icon/@BUILTIN='button_cancel'">
	<!-- don't process subtrees with an X icon at all -->
      </xsl:when>

      <!-- did the user accidently press ALT-R on a *tag node*, turning it into a richcontent node?
	   Then we nevertheless want to turn it into a plaintext node. -->
      <xsl:when test="starts-with(normalize-space(richcontent[@TYPE='NODE']/html/body), '°') ">
	<xsl:copy>
 	  <xsl:apply-templates select="@*" mode="pass0" />
	  <xsl:attribute name="TEXT">
	    <xsl:value-of select="normalize-space(richcontent[@TYPE='NODE']/html/body)" />
	  </xsl:attribute>
	  <xsl:apply-templates select="node()[name() != 'richcontent' ]" mode="pass0" />
	</xsl:copy>
      </xsl:when>

      <xsl:otherwise>
	<xsl:copy>
	  <xsl:apply-templates select="node() | @*" mode="pass0" />
	</xsl:copy>
      </xsl:otherwise>
      
    </xsl:choose>
  </xsl:template>
  

  <xsl:template match="@*" mode="pass0">
    <xsl:copy />
  </xsl:template>

  <xsl:template match="@TEXT" mode="pass0" >
    <xsl:attribute name="TEXT">
      <xsl:value-of select="normalize-space(.)" />
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="@ID" mode="pass0" >
    <xsl:attribute name="mmId">
      <xsl:value-of select="normalize-space(.)" />
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="@CREATED | @MODIFIED | @POSITION" mode="pass0" />





  <!-- $$$$$ pass0 -> pass100
       
       slice nodes like
       °S °p °-
       into
       °S
       °p
       °-
       $$$$$ -->



  <!-- pass99: as long as there is no ° node handle node tree in a traditional way,
       here: simply copy through -->

  <xsl:template match="* | text()" mode="pass99">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="* | text()" mode="pass99" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="node[starts-with(@TEXT,'°')]" mode="pass99">
    <xsl:apply-templates select="." mode="pass100" />
  </xsl:template>


  <xsl:template match="node" mode="pass100">
    <xsl:choose>

      <xsl:when test="starts-with(@TEXT,'°')">
	<xsl:apply-templates select="." mode="pass100_tag">
	  <xsl:with-param name="pString">
	    <xsl:value-of select="substring-after(@TEXT, '°')"/>
	  </xsl:with-param>
	  <xsl:with-param name="IDcount">
	    <xsl:value-of select="'1'"/>
	  </xsl:with-param>
	</xsl:apply-templates>
      </xsl:when>

      <xsl:otherwise>
	<xsl:copy>
	  <xsl:copy-of select="@*" />
	  <xsl:apply-templates select="*" mode="pass100" />
	</xsl:copy>
      </xsl:otherwise>
      
    </xsl:choose>
  </xsl:template>


  <xsl:template match="icon | richcontent" mode="pass100">
    <xsl:copy-of select="." />
  </xsl:template>




  <!-- pString starts with ° -->
  <xsl:template match="node" mode="pass100_tag">
    <xsl:param name="pString" />
    <xsl:param name="IDcount" />
    
    <xsl:variable name="pString2">
      <xsl:value-of select="normalize-space(substring-after($pString, ' '))" />
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$pString2 = ''">
	<!-- a simple tag node --> 
	<node line="175" deprecated_ID="{@deprecated_ID}.{$IDcount}">
	  <xsl:attribute name="TEXT">
	    <xsl:text>°</xsl:text>
	    <xsl:value-of select="$pString" />
	  </xsl:attribute>
	  <xsl:apply-templates select="*" mode="pass100" />
	</node>
      </xsl:when>

      <xsl:when test="$pString2 != ''">
	<!-- a tag node which may contain other tags or non tag text --> 
	<node line="190" deprecated_ID="{@deprecated_ID}.{$IDcount}">
	  <!-- debug <xsl:attribute name="pString2"><xsl:value-of select="$pString2" /></xsl:attribute> -->
	  <xsl:attribute name="TEXT">
	    <xsl:text>°</xsl:text>
	    <xsl:value-of select="normalize-space(substring-before($pString, ' '))" />
	  </xsl:attribute>

	  <xsl:choose>
	    
	    <xsl:when test="starts-with($pString2,'°')">
	      <!-- there is another tag following -->
	      <xsl:apply-templates select="." mode="pass100_tag">
		<xsl:with-param name="pString">
		  <xsl:value-of select="substring-after($pString2, '°')"/>
		</xsl:with-param>
		<xsl:with-param name="IDcount">
		  <xsl:value-of select="$IDcount+1"/>
		</xsl:with-param>
	      </xsl:apply-templates>
	    </xsl:when>

	    <xsl:otherwise>
	      <!-- the rest of the node is non tag text -->
	      <node line="214" deprecated_ID="{@deprecated_ID}.{$IDcount+1}">
		<xsl:attribute name="TEXT">
		  <xsl:value-of select="$pString2" />
		</xsl:attribute>
		<xsl:apply-templates select="*" mode="pass100" />
	      </node>
	    </xsl:otherwise>
	  </xsl:choose>
	</node>
      </xsl:when>
    </xsl:choose>
  </xsl:template>




  <!-- pass101: simple replacent mechanism
       replace a given tag, e.g.
       °n
       with a tag chain, e.g.
       °p °- °*
  -->

  <xsl:template match="* | text()" mode="pass101">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="* | text()" mode="pass101" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="node[starts-with(@TEXT,'°')]" mode="pass101">
    <xsl:choose>
      <xsl:when test="@TEXT='°n'">
	<node TEXT="°p">
	  <node TEXT="°- °*">
	    <xsl:apply-templates select="* | text()" mode="pass101" />
	  </node>
	</node>
      </xsl:when>
      <xsl:otherwise>
	<xsl:copy>
	  <xsl:copy-of select="@*" />
	  <xsl:apply-templates select="* | text()" mode="pass101" />
	</xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>



  <!-- pass100 -> pass120  -->


  <xsl:variable name="pass120_tags">
    <tag TEXT=''  priority="" tagClass="empty" /> <!-- "don't show" -->
    <tag TEXT='*' priority="" tagClass="special" />
    <tag TEXT='°' priority="" tagClass="special" />
    <tag TEXT='a' priority="" tagClass="simple" />
    <tag TEXT='A' priority="" tagClass="simple" />
    <tag TEXT='d' priority="4"  shortIdSyntax="yes" tagClass="simple" />
    <tag TEXT='D' priority="3"  shortIdSyntax="yes" tagClass="simple" />
    <tag TEXT='F' priority="1"  shortIdSyntax="yes" tagClass="simple" s1="D" />
    <tag TEXT='S' priority="2"  shortIdSyntax="yes" tagClass="simple" s1="S" />
    <tag TEXT='B' priority="4"  shortIdSyntax="yes" tagClass="simple"  />
    <tag TEXT='p' priority="4" tagClass="simple" s1="p" />
    <tag TEXT='s' priority="2"  shortIdSyntax="yes" tagClass="simple" s1="P" />
    <tag TEXT='N' priority="" tagClass="simple"  />
    <tag TEXT='n' priority="" tagClass="simple"  />
    <tag TEXT='q' priority="" tagClass="simple" s1="q" />
    <tag TEXT='r' priority="" tagClass="simple" />
    <tag TEXT='-' priority="" tagClass="list" s1="-" />
    <tag TEXT='t' priority="" tagClass="list" /> <!-- table by rows -->
    <tag TEXT='T' priority="" tagClass="list" /> <!-- table by columns -->
    <tag TEXT='c' priority="" tagClass="list" /> <!-- table cell -->
    <tag TEXT=',' priority="" tagClass="row" />
    <tag TEXT=';' priority="" tagClass="row" />
    <tag TEXT='|' priority="" tagClass="row" s1="|" />
    <tag TEXT='(' priority="" tagClass="row" s1="()" />
  </xsl:variable>




  <xsl:template match="map | node" mode="pass120">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="*" mode="pass120" />
    </xsl:copy>
  </xsl:template>


  <xsl:template match="node[starts-with(@TEXT,'°')]" mode="pass120">

    <xsl:variable name="tagChar"  select="substring(@TEXT,2,1)" />
    <xsl:variable name="tagClass" select="common:node-set($pass120_tags)/tag[@TEXT = $tagChar]/@tagClass" />
    <xsl:variable name="tagModifier" select="substring(@TEXT,4)" />
    <xsl:variable name="punctuation">
      <xsl:choose>
	<xsl:when test="not(substring(@TEXT,3,1) = '_')  ">
	  <xsl:value-of select="substring(@TEXT,3,1)" />
	</xsl:when>
      </xsl:choose>
    </xsl:variable>

    
    <node line="250"
	  tagChar="{$tagChar}"
	  tagClass="{$tagClass}"
	  punctuation="{$punctuation}"
	  tagModifier="{$tagModifier}"
	  >
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node[starts-with(@TEXT,'@')]" mode="pass120_attribute" />
      <xsl:apply-templates select="*[not(starts-with(@TEXT,'@'))]" mode="pass120" />
    </node>
  </xsl:template>


  <xsl:template match="node[starts-with(@TEXT,'@')]" mode="pass120_attribute">
    <attribute NAME="{substring-after(@TEXT, '@')}">
      <xsl:attribute name="VALUE">
	<xsl:for-each select="node">
	  <xsl:value-of select="@TEXT"/>
	  <xsl:if test="position() != last()">
	    <xsl:text>; </xsl:text>
	  </xsl:if>
	</xsl:for-each>
      </xsl:attribute>
    </attribute>
  </xsl:template>

  <xsl:template match="icon | richcontent | attribute" mode="pass120">
    <xsl:copy-of select="." />
  </xsl:template>
  <!--
      <xsl:template match="*" mode="pass120">
      <node TEXT="ERROR 120. name() = {name()}; ID={@deprecated_ID}">
      <xsl:apply-templates select="*" mode="pass120" />
      </node>
      </xsl:template>
  -->




  <!-- pass 120 -> pass150 
       auto format / distribute tag chains among children  -->



  <!-- copy through only important top level elements -->
  
  <xsl:template match="*" mode="pass149">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="*" mode="pass149" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="icon | richcontent | attribute " mode="pass149">
    <xsl:copy-of select="." />
  </xsl:template>


  <xsl:template match="node[@tagChar]" mode="pass149">
    <xsl:apply-templates select="." mode="pass150" />
  </xsl:template>

  




  <!-- the first tag node in the current subtree already occurred:
       We are now in mode="pass150" -->


  <xsl:template match="node[not(@tagChar)]" mode="pass150">
    <xsl:param name="tagcount" />
    <xsl:param name="repeat" />
    
    <xsl:choose>
      
      <xsl:when test="$tagcount = 0">
	<node>
	  <xsl:copy-of select="@*" />
	  <xsl:apply-templates select="*" mode="pass150">
	    <xsl:with-param name="tagcount" select="$tagcount" />
	    <xsl:with-param name="repeatID" select="''" />
	  </xsl:apply-templates>
	</node>
      </xsl:when>

      <xsl:otherwise>
	<!-- should not occurr, debug entry -->
	<node line="330" TEXT="ERROR 330" />
      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>


  <xsl:template match="node[@tagChar]" mode="pass150">
    <!-- switch to tag count mode -->
    <xsl:apply-templates select="." mode="pass150_tag">
      <xsl:with-param name="tagcount" select="'1'" />
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="icon | richcontent | attribute" mode="pass150">
    <xsl:copy-of select="." />
  </xsl:template>
  
  <xsl:template match="*" mode="pass150">
    <node TEXT="ERROR 150. name() = {name()};  deprecated_ID={@deprecated_ID}">
      <xsl:apply-templates select="*" mode="pass150" />
    </node>
  </xsl:template>



  <!--  mode="pass150_tag" -->

  <xsl:template match="node" mode="pass150_tag">
    <xsl:param name="tagcount" />
    <xsl:param name="repeatID" />

    <xsl:variable name="precedingTagId" select="generate-id(preceding-sibling::node[@tagChar][1])" />
    
    <xsl:choose>
      <xsl:when test="@tagChar
		      and not(ancestor::node[1]/@tagChar)">
	<!-- a new chain of one or more tag chars begins -->
	<xsl:apply-templates select="node" mode="pass150_tag">
	  <xsl:with-param name="tagcount" select="'1'" />
	  <xsl:with-param name="repeatID" select="@deprecated_ID" />
	</xsl:apply-templates>
      </xsl:when>
      
      <xsl:when test="@tagChar">
	<xsl:apply-templates select="node" mode="pass150_tag">
	  <xsl:with-param name="tagcount" select="$tagcount + 1" />
	  <xsl:with-param name="repeatID" select="@deprecated_ID" />
	</xsl:apply-templates>
      </xsl:when>

      <xsl:when test="not(@tagChar)
		      and not (ancestor::node[$tagcount]/@tagChar)
		      ">
	<!-- no more tags available at all: switch to naive mode -->
	<xsl:apply-templates select="." mode="pass150">
	  <xsl:with-param name="tagcount" select="'0'" />
	  <xsl:with-param name="repeatID" select="''" />
	</xsl:apply-templates>
      </xsl:when>
      
      <!-- core of the auto format template: emit a tagChar node to a group of non-tag nodes -->
      <xsl:when test="not(@tagChar) and
		      (not(preceding-sibling::node) or preceding-sibling::node[1][@tagChar ])">

	<!-- in order to handle "°*" tags -->
	<xsl:variable name="tagCountCorrection">
	  <xsl:choose>
	    <xsl:when test="ancestor::node[$tagcount]/@tagChar='*'
			    and ancestor::node[$tagcount+1]/@tagChar!='*'">1</xsl:when>
	    <xsl:otherwise>0</xsl:otherwise>
	  </xsl:choose>
	</xsl:variable>
	
	<node line="390_tag"> <!-- debug: tagcount="{$tagcount}" -->
	  <xsl:copy-of select="ancestor::node[$tagcount + $tagCountCorrection]/@*" />
	  <xsl:attribute name="deprecated_ID">
	    <xsl:value-of select="ancestor::node[$tagcount + $tagCountCorrection]/@deprecated_ID"/>
	    <xsl:text>.</xsl:text>
	    <xsl:value-of select="$tagcount + $tagCountCorrection" />
	  </xsl:attribute>
	  <xsl:copy-of select="ancestor::node[$tagcount + $tagCountCorrection]/attribute | ancestor::node[$tagcount + $tagCountCorrection]/icon " />

	  <!-- group nodes, switch to pass151 -->
	  <!-- . is the boss of a group -->
	  <xsl:apply-templates select=". |
				       following-sibling::node
				       [not(@tagChar)]
				       [generate-id(preceding-sibling::node[@tagChar][1]) = $precedingTagId ]"
			       mode ="pass151" >
	    <xsl:with-param name="tagcount" select="$tagcount  + $tagCountCorrection" />
	  </xsl:apply-templates>
	</node>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="*" mode="pass150_tag">
    <xsl:copy-of select="." />
  </xsl:template>



  <!-- mode="pass151" -->

  <xsl:template match="node" mode="pass151">
    <xsl:param name="tagcount" />
    <xsl:param name="repeatID" />
    
    <xsl:choose>
      
      <xsl:when test="$tagcount > 0 and ancestor::node[$tagcount - 1 ]/@tagChar ">
	<!-- there still are auto-formating tags available for the children:
	     proceed with tag mode -->
	<node line="420">
	  <xsl:copy-of select="@*" />
	  <xsl:apply-templates select="*" mode="pass150_tag">
	    <xsl:with-param name="tagcount" select="$tagcount" />
	  </xsl:apply-templates>
	</node>
      </xsl:when>

      <!--
	  <xsl:when test="$tagcount > 0 and $repeatID != '' ">
	  <!- - there are no more auto-formating tags available;
	  however, the last tag char was a "*":
          repeatID - ->
	  <node line="425">
	  <xsl:copy-of select="@*" />
	  <xsl:apply-templates select="*" mode="pass150_tag">
	  <xsl:with-param name="tagcount" select="$tagcount + 1" />
	  </xsl:apply-templates>
	  </node>
	  </xsl:when> -->

      <xsl:otherwise>
	<node line="430">
	  <xsl:copy-of select="@*" />
	  <!-- there are no more auto-formatting tags available; switch to ordinary mode -->
	  <xsl:apply-templates select="*" mode="pass150">
	    <xsl:with-param name="tagcount" select="'0'" />
	  </xsl:apply-templates>
	</node>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>




  <!-- pass500 -->
  
  <xsl:variable name="AMANFANGWAROEDNIS">
    <map>
      <node TEXT="root">
	  <node TEXT="°i">
	    <node TEXT="STDIN" />
	</node>
      </node>
    </map>
  </xsl:variable>

  <xsl:variable name="STDIN">
    <xsl:copy-of select="/" />
  </xsl:variable>

  <xsl:variable name="pass500">
    <xsl:apply-templates select="common:node-set($AMANFANGWAROEDNIS)/map" mode="pass500" />
  </xsl:variable>


  <xsl:template match="node() | @*" mode="pass500">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="pass500" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="node[@TEXT='°i']" mode="pass500">
    <xsl:apply-templates select="node" mode="pass500-include" />
  </xsl:template>



  
  <xsl:template match="node" mode="pass500-include">
    
    <xsl:variable name="READFILE">    
      <xsl:choose>
	<!-- process standard input? -->
	<xsl:when test="@TEXT = 'STDIN' ">
	  <xsl:call-template  name="message">
	    <xsl:with-param name="bias" select="'1'"/>
	    <xsl:with-param name="theMessage">pass500-include 110: STDIN</xsl:with-param>
	  </xsl:call-template>
	  <map>
	    <node TEXT="°F">
	      <xsl:copy-of select="common:node-set($STDIN)/map/node"  />
	    </node>
	  </map>
	</xsl:when>

	<xsl:otherwise>
	  <xsl:copy-of select="document(@TEXT,.)" />
	</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>


    <xsl:choose>

      	  <!-- input is html: simply encapsulate within richcontent -->
	  <xsl:when test="common:node-set($READFILE)/html">
	    <xsl:call-template  name="message">
	      <xsl:with-param name="bias" select="'1'"/>
	      <xsl:with-param name="theMessage">pass500-include 100: embedding html file <xsl:value-of select="@TEXT"  />
	      </xsl:with-param>
	    </xsl:call-template>
	    <node TEXT="°r">
	      <node>
		<richcontent TYPE='NODE'>
		  <xsl:copy-of select="common:node-set($READFILE)/html" />
		</richcontent>
	      </node>
	    </node>
	  </xsl:when>

	  <!-- input is a mindmap -->
	  <xsl:when test="common:node-set($READFILE)/map">

	    <xsl:call-template  name="message">
	      <xsl:with-param name="bias" select="'1'"/>
	      <xsl:with-param name="theMessage">pass500-include 130: included mindmap <xsl:value-of select="@TEXT"  /> with <xsl:value-of select="count(common:node-set($READFILE)//node)"  /> nodes</xsl:with-param>
	    </xsl:call-template>

	    
   	    <!-- here comes the main pipeline, via pass500 applied on each single input file -->
	    <!-- clean -->
	    <xsl:variable name="pass0">
	      <xsl:apply-templates select="common:node-set($READFILE)/map" mode="pass0"/>
	    </xsl:variable>

	    <!-- slice, replace, slice again -->
	    <xsl:variable name="pass100">
	      <xsl:apply-templates select="common:node-set($pass0)/map" mode="pass99" />
	    </xsl:variable>
	    <xsl:variable name="pass101">
	      <xsl:apply-templates select="common:node-set($pass100)/map" mode="pass101" />
	    </xsl:variable>
	    <xsl:variable name="pass102">
	      <xsl:apply-templates select="common:node-set($pass101)/map" mode="pass99" />
	    </xsl:variable>
	    
	    <!-- identify interpunction and macros; translate @attribute nodes to mm-attributes
		 DEPRECATED: generate id and label -->
	    <xsl:variable name="pass120">
	      <xsl:apply-templates select="common:node-set($pass102)/map" mode="pass120" />
	    </xsl:variable>

	    <!-- auto format: transform
		 °S °p °- °* Heading partext liLevel1 liLevel2 liLevel3
		 into
		 °S Heading °p partext  °- liLevel1 °- liLevel2 °- liLevel3	    -->
	    <xsl:variable name="pass150">
	      <xsl:call-template name="message">
		<xsl:with-param name="bias" select="'2'"/>
		<xsl:with-param name="theMessage">pass150</xsl:with-param>
	      </xsl:call-template>
	      <xsl:apply-templates select="common:node-set($pass120)/map" mode="pass149" />
	    </xsl:variable>

	    <!-- last pipeline step: start pass500 itself -->
	    <xsl:apply-templates select="common:node-set($pass150)/map/node" mode="pass500" />

	  </xsl:when>

	  
	  <xsl:otherwise>
	    <xsl:call-template  name="message">
	      <xsl:with-param name="bias" select="'1'"/>
	      <xsl:with-param name="theMessage">pass500-include 150: import doc <xsl:value-of select="@TEXT"  /> not found; traversing child tree instead</xsl:with-param>
	    </xsl:call-template>
	    <xsl:apply-templates select="node" mode="pass500" />
	  </xsl:otherwise>
	
    </xsl:choose>

  </xsl:template> <!-- pass500 include -->




  
  
  <!-- nf1 normal form 1 -->

  <xsl:variable name="nf1">
    <xsl:choose>
      <!-- restrict processing to nodes with @TEXT = external parameter $TEXT -->
      <xsl:when test="$TEXT != ''">
	<xsl:message>restrict   TEXT=<xsl:value-of select="$TEXT" /></xsl:message>
	<xsl:apply-templates select="common:node-set($pass500)/map" mode="nf1-TEXT" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:apply-templates select="common:node-set($pass500)/map" mode="nf1" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>


  <xsl:template match="/map | map/node " mode="nf1-TEXT">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node" mode="nf1-TEXT" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="node" mode="nf1-TEXT">
    <xsl:apply-templates select="node[@TEXT=$TEXT or descendant::node/@TEXT=$TEXT or ancestor-or-self::node/@TEXT=$TEXT]" mode="nf1-TEXT" />
  </xsl:template>

  <xsl:template match="node[@TEXT=$TEXT]" mode="nf1-TEXT">
    <node>
      <xsl:copy-of select="ancestor-or-self::node[@tagChar='F'][1]/@*" />
      <xsl:apply-templates select="ancestor-or-self::node[parent::node/@tagChar='F'][1]" mode="nf1" />
    </node>
  </xsl:template>


  
  
  <xsl:template match="node()" mode="nf1">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node()" mode="nf1" />
    </xsl:copy>
  </xsl:template>

  
  <xsl:template match="node" mode="nf1">
    <xsl:variable name="myMmId" select="@mmId" />
    <xsl:variable name="count">
      <xsl:value-of select="count( (preceding::node | ancestor::node)[@mmId = $myMmId] )" />
    </xsl:variable>
    
    <xsl:copy>
      <xsl:copy-of select="@*" />

      <xsl:choose>

	<xsl:when test="$count > 0">
	  <xsl:attribute name="mmId">
	    <xsl:value-of select="$myMmId" />
	    <xsl:text>_m</xsl:text>
	    <xsl:value-of select="$count" />
	  </xsl:attribute>
	</xsl:when>

	<xsl:when test="not(@mmId!='')">
	  <xsl:attribute name="mmId">
	    <xsl:value-of select="generate-id()" />
	  </xsl:attribute>
	</xsl:when>
      </xsl:choose>
      
      <xsl:choose>
	<xsl:when test="@tagChar != ''">
  	  <xsl:attribute name="BACKGROUND_COLOR">#ccffcc</xsl:attribute>	  
	</xsl:when>
      </xsl:choose>

      <xsl:apply-templates select="node()" mode="nf1" />

    </xsl:copy>
  </xsl:template>




    
  <!-- richcontent -->

  <xsl:variable name="richcontent">
    <xsl:apply-templates select="common:node-set($nf1)/map" mode="richcontent" />
  </xsl:variable>

  <xsl:template match="node()" mode="richcontent">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node()" mode="richcontent" />
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="node[@TEXT][not(@tagChar)]" mode="richcontent">
    
    <xsl:variable name="myTagChar" select="../@tagChar" />
    <xsl:variable name="shortIdSyntax" select="$pass120_tags/tag[@TEXT=$myTagChar]/@shortIdSyntax" />

    <xsl:copy>
      <xsl:copy-of select="@*[name() != 'TEXT']" />
      <richcontent TYPE="NODE">
	<html>
	  <head/>
	  <body>
	    <p line="751">
	      <xsl:call-template name="parseWikiText">
		<xsl:with-param name="shortIdSyntax" select="$shortIdSyntax" />
		<xsl:with-param name="text" select="@TEXT" />
	      </xsl:call-template>
	    </p>
	  </body>
	</html>
      </richcontent>
      <xsl:apply-templates select="node()" mode="richcontent" />
    </xsl:copy>
  </xsl:template>



  <xsl:template name="parseWikiText">

    <xsl:param name="text" />
    <xsl:param name="shortIdSyntax" />

    <xsl:variable name="string1"  select="substring-before($text, '[[')" />
    <xsl:variable name="string2"  select="substring-after($text, '[[')" />
    <xsl:variable name="string3"  select="normalize-space(substring-before($string2, ']]'))" />
    <xsl:variable name="linkGiven"     select="contains($text, '[[') and contains($string2, ']]') " />
    
    <xsl:variable name="nameORhrefGiven"     select="contains($string3, '|')" />
    <xsl:variable name="string31" select="normalize-space(substring-before($string3, '|'))" />
    <xsl:variable name="string32" select="normalize-space(substring-after($string3, '|'))" />

    <xsl:variable name="nameANDhrefGiven" select="contains($string32, '|')" />
    <xsl:variable name="string321" select="normalize-space(substring-before($string32, '|'))" />
    <xsl:variable name="string322" select="normalize-space(substring-after($string32, '|'))" />

    <xsl:variable name="string4"  select="substring-after($text, ']]')" />


    <!-- $text: "string1[[ string2                                   " -->

    <!-- linkGiven -->
    <!-- $text: "string1[[ string3                          ]]string4" -->

    <!-- nameORhrefGiven -->
    <!-- $text: "string1[[ string31 | string32              ]]string4" -->

    <!-- nameANDhrefGiven -->
    <!-- $text: "string1[[ string31 | string321 | string322 ]]string4" -->


    <xsl:variable name="triple">
      <xsl:choose>

	<xsl:when test="$nameANDhrefGiven and $string322 != '' ">
	  <xsl:variable name="name">
	    <xsl:call-template name="myEncodeForUri">
	      <xsl:with-param name="x" select="$string31" />
	    </xsl:call-template>
	  </xsl:variable>
	  <span class="triple" line="780" name="{$name}" href="{$string321}" text="{$string322}" />

	  <!-- <span class="triple" line="780" name="{$string31}" href="{$string321}" text="{$string322}" /> -->
	</xsl:when>

	<!-- lookup requested -->
	<xsl:when test="$nameANDhrefGiven and $string322 = '' ">

	  <xsl:variable name="name">
	    <xsl:call-template name="myEncodeForUri">
	      <xsl:with-param name="x" select="$string31" />
	    </xsl:call-template>
	  </xsl:variable>
	  <xsl:variable name="href">
	    <xsl:call-template name="myEncodeForUri">
	      <xsl:with-param name="x" select="$string321" />
	    </xsl:call-template>
	  </xsl:variable>
	  <span class="triple" line="780" name="{$name}" href="{$href}" text="{$string322}"/>

	  <!-- <span class="triple" line="780" name="{$string31}" href="{$string321}" text="{$string322}"/> -->
		  
	</xsl:when>

	<!-- shortIdSyntax:  string31 contains the name of the element -->
	<xsl:when test="$nameORhrefGiven and $shortIdSyntax='yes' ">
	  <!--
	  <xsl:variable name="name">
	    <xsl:call-template name="myEncodeForUri">
	      <xsl:with-param name="x" select="$string31" />
	    </xsl:call-template>
	  </xsl:variable>
	  <span class="triple" line="782" name="{$name}" text="{$string32}" />
	  -->
	  <span class="triple" line="782" name="{$string31}" text="{$string32}" />
	</xsl:when>

	<xsl:when test="$nameORhrefGiven ">
	  <span class="triple" line="783" href="{$string31}" text="{$string32}" />
	</xsl:when>
	
	<xsl:otherwise>
	  <span class="triple" line="784"  href="{$string3}" text="{$string3}" />
	</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    
    <xsl:choose>

      <xsl:when test="$linkGiven">

	<!-- emit text left of [[ ]] --> 
	<xsl:value-of select="$string1" />

	<xsl:call-template name="triple2anchor">
	  <xsl:with-param name="triple">
	    <xsl:copy-of select="$triple" />
	  </xsl:with-param>
	</xsl:call-template>

	<!-- process text right of [[ ]] --> 
	<xsl:call-template name="parseWikiText">
	  <xsl:with-param name="text" select="$string4" />
	  <xsl:with-param name="shortIdSyntax" select="''" /> <!-- short id syntax only for the first link in a node -->
	</xsl:call-template>
      </xsl:when>
      
      <!-- text without any link -->
      <xsl:otherwise>
	<!-- debug: <span class="debug" line="880"><xsl:value-of select="$text" /></span> -->
	<xsl:value-of select="$text" />
      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>

  
  <xsl:template name="triple2anchor">
    <xsl:param name="triple" />

    <a line="890" class="triple" >
      <xsl:copy-of select="$triple/span/@class" />
      <xsl:copy-of select="$triple/span/@name" />
      <xsl:copy-of select="$triple/span/@text" />
      <xsl:copy-of select="$triple/span/@href" />
      <xsl:attribute name="hrefType">
	<xsl:choose>
	  <xsl:when test="$uriScheme/s[starts-with($triple/span/@href,text())]">external</xsl:when>
	  <xsl:otherwise>internal</xsl:otherwise>
	</xsl:choose>
      </xsl:attribute>
      
      <xsl:variable name="firstMatchingLanguageTag">
	<xsl:value-of select="$lTag/l[starts-with($triple/span/@name,@tag)][1]/@tag" />
      </xsl:variable>

      <xsl:choose>
	<xsl:when test="$firstMatchingLanguageTag != '' ">
	  <xsl:attribute name="multiLanguageId">
	    <xsl:value-of select="normalize-space(substring-after($triple/span/@name,$firstMatchingLanguageTag))" />
	  </xsl:attribute>
	  <xsl:attribute name="languageTag">
	    <xsl:value-of select="$firstMatchingLanguageTag" />
	  </xsl:attribute>
	  <!-- V020: <node class="meta" name="multiLanguageId" languageTag="{$firstMatchingLanguageTag}" /> -->
	</xsl:when>
      </xsl:choose>

      <!-- text() axis of a[@triple] is only informative here, will be overwritten later -->
      <xsl:text>DEBUG_1095_</xsl:text>
      <xsl:value-of select="$triple/span/@text" />
      
    </a>
  </xsl:template>

  


  
  <!-- candidateId -->

  <xsl:variable name="candidateId">
    <xsl:apply-templates select="common:node-set($richcontent)/map" mode="candidateId" />
  </xsl:variable>
  
  <xsl:template match="node()" mode="candidateId">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node()" mode="candidateId" />
    </xsl:copy>
  </xsl:template>

  
  <xsl:template match="node[../@tagChar]" mode="candidateId">

    <xsl:variable name="myTagChar" select="../@tagChar" />
    <xsl:variable name="myPriority"    select="$pass120_tags/tag[@TEXT=$myTagChar]/@priority" />

    <xsl:variable name="shortIdSyntax" select="$pass120_tags/tag[@TEXT=$myTagChar]/@shortIdSyntax" />

    <xsl:copy>
      <xsl:copy-of select="@*" />

      <!-- generate TEXT attribute from richcontent -->
      <xsl:attribute name="TEXT">
	<xsl:value-of select="richcontent[@TYPE='NODE']/html/body" />
      </xsl:attribute>
      
      <xsl:choose>
	
	<!-- we have a °F, °D, °S etc (priority 1) or a °p (priority 2) -->
	<xsl:when test="$myPriority !='' " >
	  <xsl:choose>

	    <!-- at least one candidate id is explicitely given -->
	    <xsl:when test="richcontent[@TYPE='NODE']//a[@class='triple'][1]/@name != ''" >
	      <xsl:attribute name="candidateId">
		<xsl:value-of select="richcontent[@TYPE='NODE']//a[@class='triple'][1]/@name" />
	      </xsl:attribute>
	      <xsl:attribute name="candidateIdPriority">
		<xsl:value-of select="$myPriority" />
	      </xsl:attribute>
	      <xsl:copy-of select="richcontent[@TYPE='NODE']//a[@class='triple'][1]/@multiLanguageId" />
	      <xsl:copy-of select="richcontent[@TYPE='NODE']//a[@class='triple'][1]/@languageTag" />
	    </xsl:when>

	    <!-- though priority != '' (i.e. priority > 0) there is no candidate id explicitely given:
		 if we need a candidate id suggest one, but assign lowest priority 9 	-->
	    <xsl:when test="$shortIdSyntax = 'yes'">

	      <xsl:attribute name="candidateId">
		<xsl:choose>
		  <xsl:when test="richcontent[@TYPE='NODE']//a[@text != ''] ">
		    <xsl:call-template name="myEncodeForUri">
		      <xsl:with-param name="x">
			<xsl:value-of select="richcontent[@TYPE='NODE']//a[@text != ''][1]/@text" />
			<!--
			<xsl:for-each select="richcontent[@TYPE='NODE']//a">
			  <xsl:value-of select="@text" />
			</xsl:for-each> -->
		      </xsl:with-param>
		    </xsl:call-template>
		  </xsl:when>
		  <xsl:otherwise>
		    <xsl:call-template name="myEncodeForUri">
		      <xsl:with-param name="x" select="richcontent[@TYPE='NODE']/html/body" />
		    </xsl:call-template>
		  </xsl:otherwise>
		</xsl:choose>
	      </xsl:attribute>

	      <xsl:attribute name="candidateIdPriority">
		<xsl:text>9</xsl:text>
	      </xsl:attribute>
	    </xsl:when>

	  </xsl:choose>
	</xsl:when> <!-- test="$myPriority !='' " -->

      </xsl:choose>
      
      <xsl:apply-templates select="node()" mode="candidateId" />
    </xsl:copy>
  </xsl:template>



  
  <!-- uriEncode -->

  <xsl:variable name="uriEncode">
    <xsl:apply-templates select="common:node-set($candidateId)/map" mode="uriEncode" />
  </xsl:variable>
  
    
  <xsl:template match="node()" mode="uriEncode">
      <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node()" mode="uriEncode" />
    </xsl:copy>
  </xsl:template>

    <!--

  <xsl:template match="@candidateId | @name | @href" mode="uriEncode">
    <xsl:copy>
      <xsl:call-template name="myEncodeForUri">
	<xsl:with-param name="x" select="." />
      </xsl:call-template>
    </xsl:copy>
  </xsl:template>
  -->
  
  <xsl:template name="myEncodeForUri">
    <xsl:param name="x" />
    <xsl:variable name="firstchar" select="substring($x,1,1)" />
    <xsl:choose>
      <xsl:when test="substring-before($myEncodeForUriPositive ,$firstchar) != '' ">
	<xsl:value-of select="$firstchar" />
      </xsl:when>
      <!-- otherwise: if $x != '' -->
      <xsl:when test="$x != ''">
	<xsl:text>_</xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:if test="substring($x,2) != ''">
      <xsl:call-template name="myEncodeForUri">
	<xsl:with-param name="x" select="substring($x,2)" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>




    
  <!-- id -->

  <xsl:variable name="id">
    <xsl:apply-templates select="common:node-set($candidateId)/map" mode="id">
      <xsl:with-param name="priority" select="1" />
    </xsl:apply-templates>
  </xsl:variable>
    
  <xsl:template match="node()" mode="id">
    <xsl:param name="priority" />
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node()" mode="id" />
    </xsl:copy>
  </xsl:template>

 
  
  <xsl:template match="node[@candidateId!='']" mode="id">

    <xsl:variable name="myCandidateId" select="@candidateId" />
    <xsl:variable name="myPriority" select="@candidateIdPriority" />	

    <xsl:variable name="countLowerPriority">
      <xsl:value-of select="count( //node[@candidateId = $myCandidateId][ not(@candidateIdPriority >= $myPriority) ] )" />
    </xsl:variable>

    <xsl:variable name="countMyPriority">
      <xsl:value-of select="count( (preceding::node | ancestor::node)[@candidateId = $myCandidateId][@candidateIdPriority = $myPriority] )" />
    </xsl:variable>

    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:attribute name="id">
	<xsl:value-of select="$myCandidateId" />
	<xsl:if test="$countLowerPriority + $countMyPriority  > 0">
	  <!-- debug
	  <xsl:text>_lower</xsl:text>
	  <xsl:value-of select="$countLowerPriority" />
	  <xsl:text>_equal</xsl:text>
	  <xsl:value-of select="$countMyPriority" />
	  -->
	  <xsl:text>(</xsl:text>
	  <xsl:value-of select="$countLowerPriority + $countMyPriority" />
	  <xsl:text>)</xsl:text>
	</xsl:if>
      </xsl:attribute>
      <xsl:apply-templates select="node()" mode="id" />
    </xsl:copy>
  </xsl:template>



  <!-- mmIdRef -->

  <xsl:variable name="mmIdRef">
    <xsl:apply-templates select="common:node-set($id)/map" mode="mmIdRef" />
    <!-- V020: <xsl:apply-templates select="common:node-set($languageTags)/map" mode="mmIdRef" /> -->
  </xsl:variable>

 
  <xsl:template match="node()" mode="mmIdRef">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node()" mode="mmIdRef" />
    </xsl:copy>
  </xsl:template>


  <!-- look up @text and @mmIdRef of anchor if not given already in @text attribute --> 
  <xsl:template match="a[@class='triple']" mode="mmIdRef">
    <xsl:variable name="myHref" select="@href" />
    <xsl:variable name="mmIdRef">
      <xsl:value-of select="//node[@id = $myHref][1]/@mmId" />
    </xsl:variable>
    
    <xsl:copy>
      <xsl:copy-of select="@*" />

      <xsl:choose>
	<xsl:when test="$mmIdRef != ''">

	  <xsl:attribute name="mmIdRef">
	    <xsl:value-of select="$mmIdRef" />
	  </xsl:attribute>

	  <!-- lookup a/text().
	       Do not xsl:apply-templates here, because triple elemens only have attributes: 
	       <xsl:apply-templates select="node()" mode="mmIdRef" />
	       instead put content of @text attribute into the a element -->
	  <xsl:choose>
	    <xsl:when test="@text != ''">
	      <xsl:attribute name="lookupQuality">explicit</xsl:attribute>
	      <xsl:value-of select="@text" />
	    </xsl:when>
	    
	    <xsl:when test="//node[@id = $myHref][1]//a[@name=$myHref][1]">
	      <xsl:attribute name="lookupQuality">1</xsl:attribute>
 	      <xsl:value-of select="//node[@id = $myHref][1]//a[@name=$myHref][1]/@text" />
	    </xsl:when>
	    
	    <xsl:when test="//node[@candidateId = $myHref][1]//a[@name=$myHref][1]">
	      <xsl:attribute name="lookupQuality">2</xsl:attribute>
	      <xsl:value-of select="//node[@candidateId = $myHref][1]//a[@name=$myHref][1]/@text" />
	    </xsl:when>
	    
	    <xsl:when test="//a[@name=$myHref][1]/@text != ''">
	      <xsl:attribute name="lookupQuality">3</xsl:attribute>
	      <xsl:value-of select="//a[@name=$myHref][1]/@text" />
	    </xsl:when>
	    
	    <xsl:otherwise>
	      <xsl:attribute name="lookupQuality">notFound</xsl:attribute>
	      <xsl:text> [[WARNING1359_</xsl:text>
	      <xsl:value-of select="$myHref" />
	      <xsl:text>]] </xsl:text>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:when> <!-- test="$mmIdRef != ''" -->

	<xsl:otherwise>
	  <xsl:value-of select="@text" />
	</xsl:otherwise>

	
      </xsl:choose>
      
	
    </xsl:copy>
  </xsl:template>

  <xsl:template match="node[@id]" mode="mmIdRef">
    <xsl:variable name="myId" select="@id" />
    <xsl:variable name="myMmId" select="@mmId" />
    <xsl:variable name="myMultiLanguageId" select="@multiLanguageId" />


    <xsl:copy>
      <xsl:copy-of select="@*" />

      <xsl:if test="//node[@id = $id][2]">
	<xsl:call-template  name="message">
	  <xsl:with-param name="bias" select="'1'"/>
	  <xsl:with-param name="theMessage">WARNING 1120: @id <xsl:value-of select="@id"  /> not unique</xsl:with-param>
	</xsl:call-template>
      </xsl:if>
      
      <xsl:for-each select="richcontent[@TYPE='NODE']//a[@class='triple'][@href=$myId]">
	<attribute class="meta" name="backlinkMmIdRef" content="{ancestor::node[1]/@mmId}" />
      </xsl:for-each>

      <!-- V020 
      <xsl:for-each select="//node[@multiLanguageId = $myMultiLanguageId]"> 
	<attribute 
	      NAME="multiLanguageIdRef"
	      backlink="{$myMultiLanguageId}"
	      mmIdRef="{@mmId}"
	      id="DEBUG_1399_{@id}"
	      languageTag="{@languageTag}" />
      </xsl:for-each>
      -->
      
      <xsl:apply-templates select="node()" mode="mmIdRef" />
    </xsl:copy>
  </xsl:template>


  
  
  <!-- nf2 -->

  <xsl:variable name="nf2">
    <xsl:apply-templates select="common:node-set($mmIdRef)/map" mode="nf2" />
  </xsl:variable>
  
  <xsl:template match="node()" mode="nf2">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node()" mode="nf2" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="node[not(@tagChar)]" mode="nf2">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      
      <!-- fetch i.e. @lang and other attributes from first link -->
      <xsl:copy-of select="richcontent[@TYPE='NODE']/html/body//a[class='triple']/@*" />

      <!-- but overwrite with own @id attribute again -->
      <xsl:copy-of select="@id" />
      
      <!-- overwrite TEXT attribute a 2nd time from richcontent: now all @href-lookups are included in the text -->
      <xsl:if test="richcontent[@TYPE='NODE']">
	<xsl:attribute name="TEXT">
	  <!-- <xsl:text>DEBUG1432_</xsl:text> -->
	  <xsl:value-of select="richcontent[@TYPE='NODE']" />
	</xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="node()" mode="nf2" />
    </xsl:copy>

  </xsl:template>
  




  
  <!-- rawHtml: first raw htm version -->
  
  <xsl:variable name="rawHtml">
    <xsl:call-template name="message">
      <xsl:with-param name="bias" select="'2'"/>
      <xsl:with-param name="theMessage">rawHtml</xsl:with-param>
    </xsl:call-template>

    <html>
      <head>
	<title>
	  <xsl:value-of select="/map/node/@TEXT" />
	  <xsl:value-of select="$build" />
	</title>
      </head>
      <body>
	<xsl:apply-templates  select="common:node-set($nf2)/map" mode="rawHtml">
	  <xsl:with-param name="hdepth" select="'1'" />
	</xsl:apply-templates>
      </body>
    </html>
  </xsl:variable>

  
  <xsl:template match="node()" mode="rawHtml">
    <xsl:param name="hdepth" />
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node()" mode="rawHtml">
	<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </xsl:copy>
  </xsl:template>


  <!-- rawHtml: display stuff -->
  
  <xsl:template name="displayNOTE">
    <xsl:choose>
      <xsl:when test="richcontent[@TYPE='NOTE']"> <!-- noTe -->
	<!-- encapsulate the whole noTe with an article: thank you html5 -->
	<article class="richcontent">
	  <xsl:copy-of select="richcontent[@TYPE='NOTE']/html/body/*" />
	</article>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  
  <xsl:template name="displayInlineText" >
    <xsl:param name="line" />
    
    <xsl:variable name="myDisplayText">
      <xsl:apply-templates select="richcontent[@TYPE='NODE']/html/body" mode="displayInlineText" />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="not(richcontent[@TYPE='NODE']/html/body)">
	<xsl:message>ERROR 1222: richcontent missing; called from line=<xsl:value-of select="$line" /> </xsl:message>
      </xsl:when>
      <xsl:otherwise>	

	<xsl:copy-of select="$myDisplayText" />
	<!-- V020 <xsl:call-template name="displayLanguageTags" /> -->
	  
	<xsl:if test="../@punctuation != ''">
	  <xsl:variable name="stringEnd">
	    <xsl:value-of select="substring($myDisplayText, string-length($myDisplayText))" />
	  </xsl:variable>
	  <xsl:if test="not($stringEnd = '.'
			or $stringEnd = ';'
			or $stringEnd = ','
			or $stringEnd = ':'
			or $stringEnd = '!'
			or $stringEnd = '?')
			"> 
	    <!-- <xsl:if test="substring-before(' .;,-.!?' , $stringEnd) = '' "> -->
	    <xsl:value-of select="../@punctuation" />
	  </xsl:if>
	</xsl:if>
	
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>



  <!-- V020 only
  <xsl:template name="displayLanguageTags" >
    <xsl:variable name="myMmId" select="@mmId" />

    
    <xsl:variable name="myLanguageTags">
      <xsl:for-each select="attribute[@NAME='multiLanguageIdRef']">
	<xsl:copy-of select="." />
	<xsl:choose>
	  <xsl:when test="@mmIdRef = $myMmId">
	    <span line="1436"><xsl:value-of select="substring(@languageTag,2,2)" /></span>
	  </xsl:when>
	  <xsl:otherwise>
	    <a line="1565"
	       class="triple"
	       href="{@id}"
	       mmIdRef="{@mmIdRef}"
	       text="{@languageTag}">
	      <xsl:value-of select="substring(@languageTag,2,2)" />
	    </a>
	  </xsl:otherwise>
	</xsl:choose>
	<xsl:if test="position()!=last()">
	  <xsl:text> | </xsl:text>
	</xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:if test="$myLanguageTags != ''" >
      <span line="1459" class="languageTags">
	<xsl:text> (</xsl:text>
	<xsl:copy-of select="$myLanguageTags" />
	<xsl:text>)</xsl:text>
      </span>
    </xsl:if>
    
  </xsl:template>
  -->
  
  <xsl:template name="attributes">
    <xsl:for-each select="../attribute">
      <xsl:attribute name="{@NAME}">
	<xsl:value-of select="@VALUE" />
      </xsl:attribute>
    </xsl:for-each>
  </xsl:template>


  
  <!-- http://www.w3.org/TR/2011/WD-html5-20110525/text-level-semantics.html#text-level-semantics -->
  <xsl:template match="node()" mode="displayInlineText" >
    <xsl:apply-templates select="node() | text() " mode="displayInlineText" />
  </xsl:template>

  <xsl:template match="text()
		       | em | strong | small | s | cite | q
		       | dfn | abbr | time | code | var | samp
		       | kbd | sub | sups | i | b | u | mark
		       | ruby | rt | rp | bdi | bdo | span | br | wbr
		       | pre
		       | strike
		       "  mode="displayInlineText" priority="1">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node() | text() " mode="displayInlineText" />
    </xsl:copy>
  </xsl:template>


  
  <xsl:variable name="imageExtensions">
    <ext ending=".png" />
    <ext ending=".gif" />
    <ext ending=".jpg" />
  </xsl:variable>
  
  <xsl:template match="a[@class='triple']"  mode="displayInlineText" priority="1">
    <xsl:choose>

      <!-- directly display .png files;
	   annoying xslt1: we have to emulate ends-with($string31, $t). Generic case:
	   test="$t = substring($string31, string-length($string31) - string-length($t) +1)">
	   we only test on file extesions with len=4, e.g. ".png"  -->
      <xsl:when test="@href!= '' and @text = ''
		      and $imageExtensions/ext/@ending = substring(@href, string-length(@href) - 3)">
	<!-- [[ href | ]] : display image -->
	<a href="{@href}"><img line="1507" src="{@href}" style="width:100%" /></a>
      </xsl:when>

      <xsl:otherwise>
	<xsl:copy>
	  <xsl:copy-of select="@*" />
	  <xsl:apply-templates select="node() | text() " mode="displayInlineText" />
	</xsl:copy>
      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>




  
  <!-- fallback template for nodes without further templating specification: simply display -->

  <xsl:template match="node[not(@tagChar)]" mode="rawHtml">
    <xsl:param name="hdepth" />
	<span line="1270" >
	  <xsl:copy-of select="@*" />
	  <xsl:call-template name="displayInlineText">
	    <xsl:with-param name="line">1270</xsl:with-param>
	  </xsl:call-template>
	  <xsl:call-template name="displayNOTE" />
	  <xsl:apply-templates select="node" mode="rawHtml">
	    <xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
	  </xsl:apply-templates>
	</span>
  </xsl:template>


  
  <!-- rawHtml: the templates -->  


  
  <!-- bibliography -->
  <xsl:template match="node[@tagChar='B']" mode="rawHtml">
    <xsl:param name="hdepth" />
    <div>
      <xsl:copy-of select="@*" />
      <dl>
	<xsl:call-template name="attributes"/>
	<xsl:apply-templates select="node" mode="rawHtml-B">
	  <xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
	</xsl:apply-templates>
      </dl>
    </div>
  </xsl:template>

  <xsl:template match="node" mode="rawHtml-B">
    <xsl:param name="hdepth" />
    <dt>
      <xsl:copy-of select="@*" />
      <xsl:call-template name="displayInlineText" />
    </dt>
    <dd>
      <xsl:apply-templates select="node" mode="rawHtml">
	<xsl:with-param name="hdepth">
	  <xsl:value-of select="$hdepth" />
	</xsl:with-param>
      </xsl:apply-templates>
    </dd>
  </xsl:template>
  

  
  <!-- ° "continue" tag: continue parent element -->
  <xsl:template match="node[@tagChar='°']" mode="rawHtml">
    <xsl:param name="hdepth" />
    <xsl:apply-templates select="node" mode="rawHtml-continue">
      <xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="node" mode="rawHtml-continue">
    <xsl:param name="hdepth" />
    <!-- we use a unix type ^L line feed here, not ^M #13 carriage return:
    not to be displayed in rendered html, but to make html output more readable. -->
    <xsl:text>&#10;</xsl:text> 
    <xsl:call-template name="displayInlineText" />
    <xsl:call-template name="displayNOTE" />
    <xsl:apply-templates select="node" mode="rawHtml">
      <xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>


  
  <!-- div elements -->
  <xsl:template match="node[@tagChar='D' or @tagChar='d']" mode="rawHtml">
    <xsl:param name="hdepth" />
    <div>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node" mode="rawHtml-div">
	<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </div>
  </xsl:template>

  <xsl:template match="node" mode="rawHtml-div">
    <xsl:param name="hdepth" />
    <div>
      <xsl:copy-of select="@*" />
      <xsl:call-template name="attributes"/>
      <xsl:apply-templates select="node" mode="rawHtml">
	<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </div>
  </xsl:template>


  
  <!-- empty tag: display:none  -->

  <xsl:template match="node[@tagChar='']" mode="rawHtml">
    <xsl:param name="hdepth" />
    <xsl:apply-templates select="node" mode="rawHtml-empty">
      <xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>


  <xsl:template match="node" mode="rawHtml-empty">
    <xsl:param name="hdepth" />
    <span style="display:none">
      <xsl:copy-of select="@*" />
      <xsl:call-template name="displayInlineText" />
    </span>
    <xsl:apply-templates select="node" mode="rawHtml">
      <xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>
  

  <!-- file -->
  <xsl:template match="node[@tagChar='F']" mode="rawHtml">
    <xsl:param name="hdepth" />
    <xsl:choose>

      <!-- first file in mindmap  or multiple files suported -->
      <xsl:when test="not(ancestor::node[@tagChar='F']) or ($multipleFiles = 'yes' and system-property('xsl:version') = '2.0' )">
	<xsl:apply-templates select="node" mode="rawHtml-file">
	  <xsl:with-param name="hdepth"><xsl:value-of select="'1'" /></xsl:with-param>
	</xsl:apply-templates>
      </xsl:when>

      <!-- treat F like an S -->
      <xsl:otherwise>
	<xsl:apply-templates select="node" mode="rawHtml-section">
	  <xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
	</xsl:apply-templates>
      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>


  <xsl:template match="node" mode="rawHtml-file">
    <xsl:param name="hdepth" />
    <div title="{@TEXT}" type="file" content="html" >
      <xsl:copy-of select="@*" />

      <xsl:choose>
	<xsl:when test="richcontent[@TYPE='NODE']/html/body//a[@class='triple']">
	  <xsl:attribute name="shortTitle">
	      <xsl:value-of select="richcontent[@TYPE='NODE']/html/body//a[@class='triple'][1]/text()" />
	  </xsl:attribute>
	</xsl:when>
      </xsl:choose>
      
      <xsl:attribute name="config">
	<xsl:value-of select="ancestor::node[../@tagChar='a'][1]/@TEXT" />
      </xsl:attribute>

      <xsl:apply-templates select="node" mode="rawHtml">
	<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </div>
  </xsl:template>





  <!-- list item -->
  <xsl:template match="node[@tagChar='-']" mode="rawHtml">
    <xsl:param name="hdepth" />
    <xsl:choose>
      <xsl:when test="node">
	<ul>
	  <xsl:copy-of select="@*" />
	  <xsl:call-template name="attributes"/>
	  <xsl:apply-templates select="node" mode="rawHtml-ul">
	    <xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
	  </xsl:apply-templates>
	</ul>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="node" mode="rawHtml-ul">
    <xsl:param name="hdepth" />
    <li>
      <xsl:copy-of select="@*" />
      <xsl:call-template name="displayInlineText" />
      <xsl:call-template name="displayNOTE" />
      <xsl:apply-templates select="node" mode="rawHtml">
        <xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </li>
  </xsl:template>



  <!-- neat paragraphs ("das Kleingedruckte") -->
  <xsl:template match="node[@tagChar='n']" mode="rawHtml">
    <xsl:param name="hdepth" />
    <div>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node" mode="rawHtml-n">
	<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </div>
  </xsl:template>

  <xsl:template match="node" mode="rawHtml-n">
    <xsl:param name="hdepth" />
    <p class="small" style="border:1px #aaa dotted;">
      <xsl:copy-of select="@*" />
      <xsl:call-template name="attributes"/>
      <xsl:call-template name="displayInlineText" />
      <xsl:call-template name="displayNOTE" />
      <xsl:apply-templates select="node" mode="rawHtml">
	<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </p>
  </xsl:template>

  
  <!-- mosaic -->
  <xsl:template match="node[@tagChar='m']" mode="rawHtml">
    <xsl:param name="hdepth" />

    <xsl:variable name="myMmId" select="@mmId" />
    <xsl:variable
	name="maxDepth"
	select="max(descendant::node[not(node)]/count(ancestor::node[not(descendant::node[ @mmId =$myMmId ])]))"/>

    <table maxDepth="{$maxDepth}" border="1">
      <xsl:call-template name="attributes"/>
      <xsl:apply-templates select="node" mode="rawHtml-m">
	<xsl:with-param name="maxDepth"><xsl:value-of select="$maxDepth" /></xsl:with-param>
	<xsl:with-param name="tr">yes</xsl:with-param>
      </xsl:apply-templates>

    </table>

  </xsl:template>

  
  <xsl:template match="node" mode="rawHtml-m">
    <xsl:param name="maxDepth" />
    <xsl:param name="tr" />
    
    <xsl:variable name="rowspan" select="count(descendant::node[not(node)])" />

    <xsl:variable name="m">
      <td  valign="top">
	<xsl:if test="$rowspan > 1">
	  <xsl:attribute name="rowspan">
	    <xsl:value-of select="$rowspan"/>
	  </xsl:attribute>
	</xsl:if>
	<xsl:if test="$maxDepth > 1 and not(node)">
	  <xsl:attribute name="colspan">
	    <xsl:value-of select="$maxDepth"/>
	  </xsl:attribute>
	</xsl:if>
	<xsl:call-template name="displayInlineText" />
	<xsl:call-template name="displayNOTE" />
      </td>
      <xsl:apply-templates select="node[1]" mode="rawHtml-m">
	<xsl:with-param name="maxDepth"><xsl:value-of select="$maxDepth - 1" /></xsl:with-param>
	<xsl:with-param name="tr">no</xsl:with-param>
      </xsl:apply-templates>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$tr='yes'">
	<tr>
	  <xsl:copy-of select="$m" />
	</tr>
      </xsl:when>
      <xsl:otherwise>
	<xsl:copy-of select="$m" />
      </xsl:otherwise>
    </xsl:choose>
    
    <xsl:apply-templates select="node[ position() > 1 ]" mode="rawHtml-m">
      <xsl:with-param name="maxDepth"><xsl:value-of select="$maxDepth - 1" /></xsl:with-param>
      <xsl:with-param name="tr">yes</xsl:with-param>
    </xsl:apply-templates>
    
  </xsl:template>

  


  <!-- paragraphs -->
  <xsl:template match="node[@tagChar='p']" mode="rawHtml">
    <xsl:param name="hdepth" />
    <div>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node" mode="rawHtml-paragraph">
	<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </div>
  </xsl:template>

  <xsl:template match="node" mode="rawHtml-paragraph">
    <xsl:param name="hdepth" />
    <p>
      <xsl:copy-of select="@*" />
      <xsl:call-template name="attributes"/>
      <xsl:call-template name="displayInlineText" />
      <xsl:call-template name="displayNOTE" />
      <xsl:apply-templates select="node" mode="rawHtml">
	<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </p>
  </xsl:template>

  

  <!-- rich content -->

  <xsl:template match="node[@tagChar='r']" mode="rawHtml">
    <xsl:param name="hdepth" />
    <div>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node" mode="rawHtml-r">
	<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </div>
  </xsl:template>

  
  <xsl:template match="node" mode="rawHtml-r">
    <xsl:param name="hdepth" />
    <article class="richcontent">
      <xsl:copy-of select="@*" />
      <xsl:call-template name="attributes"/>
      <xsl:copy-of select="richcontent[@TYPE='NODE']/html/body/node() | richcontent[@TYPE='NODE']/html/body/text()"  />
      <xsl:copy-of select="richcontent[@TYPE='NOTE']/html/body/node()  | richcontent[@TYPE='NOTE']/html/body/text()"  />
    </article>
    <xsl:apply-templates select="node" mode="rawHtml">
      <xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>



  <!-- paragraphs with section head -->
  <xsl:template match="node[@tagChar='s']" mode="rawHtml">
    <xsl:param name="hdepth" />
    <div>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node" mode="rawHtml-s">
	<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </div>
  </xsl:template>

  <xsl:template match="node" mode="rawHtml-s">
    <xsl:param name="hdepth" />
    <div>
      <xsl:copy-of select="@*" />
      <xsl:call-template name="attributes"/>
      <h6>
	<xsl:call-template name="displayInlineText" />
      </h6>
      <xsl:call-template name="displayNOTE" />
      <xsl:apply-templates select="node" mode="rawHtml">
	<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </div>
  </xsl:template>



  

  <!-- sections -->
  <xsl:template match="node[@tagChar='S']" mode="rawHtml">
    <xsl:param name="hdepth" />
    <div>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node" mode="rawHtml-section">
	<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </div>
  </xsl:template>

  <xsl:template match="node" mode="rawHtml-section">
    <xsl:param name="hdepth" />

    <xsl:variable name="htmlHeading">
      <xsl:text>h</xsl:text>
      <xsl:value-of select="$hdepth" />
    </xsl:variable>

    <section>
      <xsl:copy-of select="@*" />
      <xsl:call-template name="attributes"/>
      <xsl:element name="{$htmlHeading}">
	<xsl:value-of select="@TEXT" />
	<!-- V020 <xsl:call-template name="displayLanguageTags" /> -->
      </xsl:element>      
      <xsl:apply-templates select="node" mode="rawHtml">
	<xsl:with-param name="hdepth">
	  <xsl:value-of select="$hdepth + 1" />
	</xsl:with-param>
      </xsl:apply-templates>
    </section>
    
  </xsl:template>




  

  <!-- table by rows or colums -->
  <xsl:template match="node[@tagChar='t' or @tagChar='T']" mode="rawHtml">
    <xsl:param name="hdepth" />
    <xsl:variable name="myId" select="generate-id(.)" />

    <xsl:variable name="tableCellOrder">
      <cells>
	<xsl:for-each select="node[1]/descendant::node
			      [@tagChar='c']
      			      [generate-id(ancestor::node[@tagChar='t' or @tagChar='T'][1]) = $myId ]
			      /node">
	  <cell><xsl:value-of select="@TEXT" /></cell>
	  <!-- TBD: generate distict values, solution: http://stackoverflow.com/questions/11106331/xslt-1-0-variant-for-distinct-values -->
	</xsl:for-each>
      </cells>
    </xsl:variable>

    <xsl:variable name="allTableCells">
      <cells>
	<xsl:for-each select=".//node
			      [@tagChar='c']
      			      [generate-id(ancestor::node[@tagChar='t' or @tagChar='T'][1]) = $myId ]
			      /node">
	  <cell><xsl:value-of select="@TEXT" /></cell>
	</xsl:for-each>
      </cells>
    </xsl:variable>

    
    <xsl:choose>
      
      <!-- table by rows -->
      <xsl:when test="@tagChar='t'"> 
	<table>
	  <xsl:copy-of select="@*" />
	  <xsl:call-template name="attributes"/>
	  <xsl:apply-templates select="node" mode="rawHtml-tableRow">
	    <xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
	    <xsl:with-param name="parentTagChar"><xsl:value-of select="@tagChar" /></xsl:with-param>
	    <xsl:with-param name="tableCellOrder">
	      <xsl:copy-of select="common:node-set($tableCellOrder)" />
	    </xsl:with-param>
	  </xsl:apply-templates>
	</table>
      </xsl:when>

      <!-- table by columns -->
      <xsl:when test="@tagChar='T'">
	<table >
	  <xsl:copy-of select="@*" />
	  <xsl:call-template name="attributes"/>
	  <tr>
	    <xsl:copy-of select="@*" />
	    <xsl:apply-templates select="node" mode="rawHtml-tableColumnHead">
	      <xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
	      <xsl:with-param name="parentTagChar"><xsl:value-of select="@tagChar" /></xsl:with-param>
	      <xsl:with-param name="tableCellOrder">
		<xsl:copy-of select="common:node-set($tableCellOrder)" />
	      </xsl:with-param>
	    </xsl:apply-templates>
	  </tr>

	  <xsl:variable name="myId" select="generate-id()" />    <!-- the current node is the one with the °R -->

	  <!--
	      {myId=<xsl:value-of select="$myId" />-<xsl:value-of select="@TEXT" />;
	      myParentsId=<xsl:value-of select="$myParentsId" />-<xsl:value-of select="../@TEXT" />;
	      myGrandParentsId=<xsl:value-of select="$myGrandParentsId" />-<xsl:value-of select="../../@TEXT" />} 
	  -->

	  <xsl:for-each select="$allTableCells/cells/cell" >
	    <xsl:variable name="myCellKey" select="./text()" />
	    <xsl:if test="not(preceding::cell[text() = $myCellKey])">
	      <!-- create for each cell key one row -->
	      <tr  cellKey="{$myCellKey}">

		<!-- create for each child of the °T node a td -->
		<xsl:for-each
		    select="common:node-set($nf2)
			    //node
			    [generate-id() = $myId]
			    /node
			    ">
		  <td>
		    <!-- fill in the td with the respective values, if present -->
		    <xsl:variable name="childId" select="generate-id()" />    <!-- "I" am now the child of the node with the °T -->
		    <xsl:for-each
			select="descendant::node
				[@tagChar='c']
				[generate-id(ancestor::node[parent::node[@tagChar='t' or @tagChar = 'T']][1]) = $childId ]
				/node
				[@TEXT = $myCellKey]
				">

		      <xsl:apply-templates select="node" mode="rawHtml">
			<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
		      </xsl:apply-templates>

		    </xsl:for-each>
		  </td>
		</xsl:for-each>
	      </tr>
	    </xsl:if>
	  </xsl:for-each>
	</table>
      </xsl:when>
    </xsl:choose>
  </xsl:template>



  <xsl:template match="node" mode="rawHtml-tableRow">
    <xsl:param name="hdepth" />
    <xsl:param name="parentTagChar" />
    <xsl:param name="tableCellOrder" />

    <xsl:variable name="thOrTd">
      <xsl:choose>
	<xsl:when test="position() = 1">th</xsl:when>
	<xsl:otherwise>td</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <tr tableCellOrder="{$tableCellOrder}">
      <xsl:element name="{$thOrTd}">
	<xsl:copy-of select="@*" />
	<xsl:attribute name="line">1478</xsl:attribute>

	<xsl:call-template name="displayInlineText" />
	<xsl:call-template name="displayNOTE" />
	<xsl:apply-templates select="node" mode="rawHtml">
	  <xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
	</xsl:apply-templates>
      </xsl:element>
      
      <xsl:variable name="myGrandParentsId" select="generate-id(../../node)" />
      <xsl:variable name="myParentsId" select="generate-id(../node)" />  <!-- the node with the °t or °T -->
      <xsl:variable name="myId" select="generate-id()" />  <!-- the specific row we are handling currently -->

      <xsl:for-each select="$tableCellOrder/cells/cell" >
	<xsl:variable name="myCellKey" select="./text()" />
	<xsl:if test="not(preceding::cell[text() = $myCellKey])">
	  <xsl:for-each
	      select="common:node-set($nf2)
		      //node
		      [generate-id() = $myId]
		      //node
		      [@tagChar='c']
		      [generate-id(ancestor::node[parent::node[@tagChar='t' or @tagChar='T']][1]) = $myId ]

		      ">
	    <!--
		<xsl:choose>
		<xsl:when test="node[@TEXT = $myCellKey]/node">
	    -->
	    <xsl:element name="{$thOrTd}">
	      <xsl:copy-of select="@*" />
	      <xsl:attribute name="line">1596</xsl:attribute>
	      <xsl:apply-templates select="node[@TEXT = $myCellKey]/node" mode="rawHtml">
		<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
	      </xsl:apply-templates>
	    </xsl:element>
	    <!--
		</xsl:when>
		</xsl:choose>
	    -->
	  </xsl:for-each>
	</xsl:if>
      </xsl:for-each>
    </tr>
    
  </xsl:template>


  <xsl:template match="node" mode="rawHtml-tableColumnHead">
    <xsl:param name="hdepth" />
    <xsl:param name="parentTagChar" />
    <xsl:param name="tableCellOrder" />
    <!-- debug {<xsl:copy-of select="$tableCellOrder" />} -->
    <th>
      <xsl:copy-of select="@*" />
      <xsl:call-template name="displayInlineText" />
      <xsl:call-template name="displayNOTE" />
      <xsl:apply-templates select="node" mode="rawHtml">
	<xsl:with-param name="hdepth"><xsl:value-of select="$hdepth" /></xsl:with-param>
      </xsl:apply-templates>
    </th>    
  </xsl:template>

  <!-- table cell -->
  <xsl:template match="node[@tagChar='c']" mode="rawHtml">
    <!-- do nothing: mode rawHtml-tableCell will be invoked by ancestor 't' or 'T' node -->
  </xsl:template>




  <!-- tagModifier -->  

  <xsl:variable name="tagModifier">
    <xsl:apply-templates select="common:node-set($rawHtml)/html" mode="tagModifier" />
  </xsl:variable>
  
  <xsl:template match="node()" mode="tagModifier">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node()" mode="tagModifier" />
    </xsl:copy>
  </xsl:template>


  <!-- no tagModifier given: simply clean from div element, proceed -->
  <xsl:template match="div[@tagModifier='']" mode="tagModifier">
    <xsl:apply-templates select="node()" mode="tagModifier" />
  </xsl:template>

  
  <!-- grid elements -->

  <xsl:variable name="ym-grids-defs">

    <!-- 2 columns-->
    <grid rowSequence="11" rowSequenceV1="1-1">
      <div class="ym-g50 ym-gl"/>
      <div class="ym-g50 ym-gr"/>
    </grid>

    <!-- 3 columns -->
    <grid rowSequence="111" rowSequenceV1="1-1-1">
      <div class="ym-g33 ym-gl"/>
      <div class="ym-g33 ym-gl"/>
      <div class="ym-g33 ym-gr"/>
    </grid>
    <grid rowSequence="12" rowSequenceV1="1-2">
      <div class="ym-g33 ym-gl"/>
      <div class="ym-g66 ym-gr"/>
    </grid>
    <grid rowSequence="21" rowSequenceV1="2-1">
      <div class="ym-g66 ym-gl"/>
      <div class="ym-g33 ym-gr"/>
    </grid>

    <!-- 4 columns -->
    <grid rowSequence="1111" rowSequenceV1="1-1-1-1">
      <div class="ym-g25 ym-gl"/>
      <div class="ym-g25 ym-gl"/>
      <div class="ym-g25 ym-gl"/>
      <div class="ym-g25 ym-gr"/>
    </grid>    
    <grid rowSequence="112" rowSequenceV1="1-1-2">
      <div class="ym-g25 ym-gl"/>
      <div class="ym-g25 ym-gl"/>
      <div class="ym-g50 ym-gr"/>
    </grid>
    <grid rowSequence="121" rowSequenceV1="1-2-1">
      <div class="ym-g25 ym-gl"/>
      <div class="ym-g50 ym-gl"/>
      <div class="ym-g25 ym-gr"/>
    </grid>
    <grid rowSequence="211" rowSequenceV1="2-1-1">
      <div class="ym-g50 ym-gl"/>
      <div class="ym-g25 ym-gl"/>
      <div class="ym-g25 ym-gr"/>
    </grid>
    <grid rowSequence="22" rowSequenceV1="2-2">
	 <div class="ym-g50 ym-gl"/>
	 <div class="ym-g50 ym-gr"/>
    </grid>
    <grid rowSequence="13" rowSequenceV1="1-3">
      <div class="ym-g25 ym-gl"/>
      <div class="ym-g75 ym-gr"/>
    </grid>
    <grid rowSequence="31" rowSequenceV1="3-1">
      <div class="ym-g75 ym-gl"/>
      <div class="ym-g25 ym-gr"/>
    </grid>

    <!-- 5 columns: Emumeration ist not shorter any more than an
         algorithm.  Is anybody out there to write the recursive
         function which enumerates all possible perturbations? -->
  </xsl:variable>


  <xsl:template match="div[starts-with(@tagModifier, 'G') or starts-with(@tagModifier, 'g') ]" mode="tagModifier">      
    <xsl:variable name="rowSequence">
      <xsl:value-of select="substring(@tagModifier,2)" />
    </xsl:variable>

    <xsl:variable name="linearize-level">
      <xsl:choose>
	<!-- switches earlier, i.e. only is a grid in wide viewports -->
	<xsl:when test="@tagModifier='G'">1</xsl:when>
	<!-- switches later, i.e. remains a grid also in smaller viewports -->
	<xsl:otherwise>2</xsl:otherwise>             
      </xsl:choose>
    </xsl:variable>
    
    <div  line="1832" class="yml-grid ym-equalize linearize-level-{$linearize-level}"
	  rowSequence="{$rowSequence}"
	  tagModifier="{@tagModifier}" >
      <xsl:apply-templates select="*" mode="tagModifier-grid-widthElement">
	<xsl:with-param name="rowSequence" select="$rowSequence" />
      </xsl:apply-templates>
    </div>
  </xsl:template>

  <xsl:template match="*" mode="tagModifier-grid-widthElement">
    <xsl:param name="rowSequence" />
    <xsl:variable name="consecutiveNumber" select="count(preceding-sibling::*)+1" />
    
    <div consecutiveNumber="{$consecutiveNumber}" rowSequence="{$rowSequence}" > <!-- debug attributes only -->
      <xsl:copy-of select="common:node-set($ym-grids-defs)/grid[@rowSequence=$rowSequence]/div[$consecutiveNumber]/@class" />
      <div>
	<xsl:call-template name="attributes"/>
	<xsl:choose>
	  <xsl:when test="position()=1">
	    <xsl:attribute name="class">ym-gbox-left</xsl:attribute>
	  </xsl:when>
	  <xsl:when test="position()=last()">
	    <xsl:attribute name="class">ym-gbox-right</xsl:attribute>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:attribute name="class">ym-gbox</xsl:attribute>
	  </xsl:otherwise>
	</xsl:choose>
	<xsl:apply-templates select="." mode="tagModifier" />
      </div>
    </div>
  </xsl:template> 


  
  <!-- EE: extended example -->
  <xsl:template match="div[@tagModifier = 'EE' ] " mode="tagModifier">      
    <xsl:variable name="myMmId" select="@mmId" />
    <table border="1" line="1851">
      <xsl:call-template name="attributes"/>
	    
      <!-- <caption>EE</caption> -->  <!-- <a href="{$myMmId}.html"><xsl:value-of select="$myMmId"/></a>-->
      <tr>
	<th>Input: Mindmap</th>
	<th>Output: html</th>
      </tr>
      <tr>
	<td>
	  <ul  class="EE" style="list-style-type:none">
	    <xsl:apply-templates  select="common:node-set($nf1)//node[@ID=$myMmId]"  mode="tagModifier-EE" />
	  </ul>
	</td>
	<td>
	  <xsl:apply-templates select="node()" mode="tagModifier" />
	</td>
      </tr>
    </table>
  </xsl:template>
  
  <xsl:template match="*" mode="tagModifier-EE">
    <li>
      <span>	    <xsl:value-of select="@TEXT" />
      <!--
	  <xsl:choose>
	  <xsl:when test="starts-with(@TEXT, '°')">
	  <xsl:value-of select="substring(@TEXT,1,3)" />
	  </xsl:when>
	  <xsl:otherwise>
	  <xsl:value-of select="@TEXT" />
	  </xsl:otherwise>
	  </xsl:choose> -->
      </span> 
      <xsl:if test="node">
	<ul style="list-style-type:none">
	  <xsl:apply-templates select="node" mode="tagModifier-EE" />
	</ul>
      </xsl:if>
    </li>
  </xsl:template>




  <xsl:template match="div[@tagModifier='MMC']/div" priority="1" mode="tagModifier">
    <xsl:variable name="myMmId" select="@mmId" />
    <xsl:variable name="MMC-XML">
      <quiz>
	<xsl:apply-templates select="common:node-set($nf2)//node[@mmId=$myMmId]//node[starts-with(icon/@BUILTIN, 'full-')]" mode="questionTextCategory" />
      </quiz>
    </xsl:variable>
    <xsl:copy-of select="$MMC-XML" />
    <xsl:apply-templates select="$MMC-XML" mode="MMC-XML2html" />
  </xsl:template>


  





  
  <!-- fileHref: relevant only for semAuth2 -->

  <xsl:variable name="fileHref">
    <xsl:apply-templates select="common:node-set($tagModifier)/html" mode="fileHref" />
  </xsl:variable>
  
  <xsl:template match="node()" mode="fileHref">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node()" mode="fileHref" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="a[@class='triple']" mode="fileHref">
    <xsl:variable name="myFileId">
      <xsl:value-of select="ancestor::div[@type='file'][1]/@id" />
    </xsl:variable>
    <xsl:variable name="mmIdRef">
      <xsl:value-of select="@mmIdRef" />
    </xsl:variable>
    <xsl:variable name="targetFileId">
      <xsl:value-of select="//*[@mmId=$mmIdRef ]/ancestor-or-self::div[@type='file'][1]/@id" />
    </xsl:variable>

    <!-- <xsl:message>TARGET FILE ID: <xsl:value-of select="$targetFileId" />  </xsl:message> -->
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:choose>
	<xsl:when test="$myFileId != $targetFileId and $targetFileId !='' ">
	  <xsl:attribute name="fileHref">
	    <xsl:value-of select="$targetFileId" />
	    <xsl:value-of select="$htmlExt" />
	  </xsl:attribute>
	</xsl:when>
      </xsl:choose>
      <xsl:apply-templates select="node()" mode="fileHref" />
    </xsl:copy>
  </xsl:template>
  



  

  <!-- fileHref -> siteMap  -->

  
  <!--      siteMap: combination of sitemap and file specific TOC -->
  <xsl:variable name="TOC">
    <xsl:apply-templates  select="common:node-set($fileHref)/html" mode="siteMap" />
  </xsl:variable>

  <xsl:template match="*" mode="siteMap">
    <xsl:apply-templates select="*" mode="siteMap" />
  </xsl:template>

  <xsl:template match="div[@type='file']" mode="siteMap">
    <xsl:variable name="myGenId">
      <xsl:value-of select="generate-id()" />
    </xsl:variable>
    <xsl:copy>
      <xsl:copy-of select="@*" />

      <!-- generate file specific TOC -->
      <xsl:apply-templates select="*" mode="siteMapTOC" />

      <!-- traverse file tree -->
      <xsl:apply-templates
	  select="descendant::div[@type='file']
		  [generate-id(ancestor::div[@type='file'][1]) = $myGenId]" mode="siteMap" />
    </xsl:copy>
  </xsl:template>


  <!-- generate file specific table of contents -->
  <xsl:template match="*" mode="siteMapTOC">
    <xsl:apply-templates select="*" mode="siteMapTOC" />
  </xsl:template>

  <xsl:template match="section" mode="siteMapTOC">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="*" mode="siteMapTOC" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="div[@type='file']" mode="siteMapTOC">
    <!-- stop traversing; subsequent sections belong to child file -->
  </xsl:template>



    

  <!-- pass1000 -->

  <xsl:variable name="pass1000">
    <xsl:apply-templates select="common:node-set($fileHref)/html" mode="pass1000" />
  </xsl:variable>
  
  <xsl:template match="node()" mode="pass1000">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node()" mode="pass1000" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="a[@class='triple'
		       and not(@fileHref != '')
		       and @href =''
		       ]" mode="pass1000">
    <span line="1263">
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="node()" mode="pass1000" />
    </span>
  </xsl:template>

  
  <!-- only semAuth2 -->
  <xsl:template match="a[@class='triple' and @fileHref!='' ]" mode="pass1000">
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:attribute name="siteHref">
	<xsl:value-of select="@fileHref" />
	<xsl:text>#</xsl:text>
	<xsl:value-of select="@href" />
      </xsl:attribute>
      <xsl:apply-templates select="node()" mode="pass1000" />
    </xsl:copy>
  </xsl:template>


  


  <!-- $$$$$
       mode="templating"
       $$$$$ -->

  <!-- the following htmlDefaultTemplate is helpful
       - for debugging ends in case of there is no other template available
       - as a showcase which macros there are available
       - as a starting point to develop own htmlTemplate
  --> 
  <xsl:variable name="htmlDefaultTemplate">
    <html>
      <head>
	<meta source="semAuthLite.xsl > htmlDefaultTemplate" />
	<title semAuth="pageTitle" />
      </head>
      <body>
	<h1 semAuth="pageTitle" />
	<h2>Macros</h2>
	<table border="1">
	  <tr>
	    <td>pageTitle</td>
	    <td><span semAuth="pageTitle" ></span></td>
	  </tr>
	  <tr>
	    <td>breadcrumb</td>
	    <td><span semAuth="breadcrumb" /></td>
	  </tr>
	  <tr>
	    <td>vNav</td>
	    <td><div semAuth="vNav" /></td>
	  </tr>
	  <tr>
	    <td>TOC</td>
	    <td><div semAuth="TOC" /></td>
	  </tr>
	</table>
	<h2>pageContent</h2>
	<table border="1"><tr><td>
	  <div semAuth="pageContent">This text element will be replaced with the pages content</div>
	</td></tr></table>
      </body>
    </html>
  </xsl:variable>



  <xsl:variable name="htmlTemplate">

    <xsl:variable name="fileTemplateLocation">
      <xsl:copy-of select="common:node-set(document($templateLocation,.))"/>
    </xsl:variable>
    
    <xsl:choose>

      <!-- there ist at least a /html/body element: import will be considered successful -->
      <xsl:when test="common:node-set($fileTemplateLocation)/html/body">
	<xsl:call-template  name="message"><xsl:with-param name="bias" select="'1'"/>
	<xsl:with-param name="theMessage">htmlTemplate: imported template <xsl:value-of select="$templateLocation"  />: <xsl:value-of select="count( common:node-set($fileTemplateLocation)//@semAuth)"  /> macros <!-- <xsl:for-each select="common:node-set($fileTemplateLocation)//@semAuth">
	<xsl:value-of select="." />; </xsl:for-each>--></xsl:with-param>
      </xsl:call-template>
	<xsl:copy-of select="common:node-set($fileTemplateLocation)"/>
      </xsl:when>

      <!-- sth. went wrong, most often: file not found -->	
      <xsl:otherwise>
	<xsl:call-template  name="message"><xsl:with-param name="bias" select="'1'"/>
        <xsl:with-param name="theMessage">Warning 241: failed to import <xsl:value-of select="$templateLocation"  />: using default $htmlDefaultTemplate instead.</xsl:with-param></xsl:call-template>
	<xsl:copy-of select="common:node-set($htmlDefaultTemplate)"/> <!-- xslt dir -->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>




  <!-- fileArray -->
  
  <xsl:variable name="fileArray">
    <xsl:call-template  name="message"><xsl:with-param name="bias" select="'2'"/>
    <xsl:with-param name="theMessage">
      <xsl:text>fileArray: filling $fileArray: </xsl:text>
      <xsl:for-each select="common:node-set($pass1000)//div[@type='file']">
	<xsl:text> "</xsl:text>
	<xsl:value-of select="@id"  />
	<xsl:text>"; </xsl:text>
      </xsl:for-each>
    </xsl:with-param></xsl:call-template>

    <xsl:choose>
      <xsl:when test="not(common:node-set($pass1000)//div[@type='file'])">
	<xsl:call-template  name="message"><xsl:with-param name="bias" select="'0'"/>
        <xsl:with-param name="theMessage">Error 1901: $pass1000 can't find div[@type='file']</xsl:with-param></xsl:call-template>
      </xsl:when>
      <xsl:when test="system-property('xsl:version') = '2.0'">
	<xsl:apply-templates select="common:node-set($pass1000)//div[@type='file']" mode="startTemplating" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:apply-templates select="common:node-set($pass1000)//div[@type='file'][1]" mode="startTemplating" />
      </xsl:otherwise>
    </xsl:choose>
    </xsl:variable>

  
  <xsl:template match="/ | node()" mode="startTemplating">
    <xsl:text>Error 74: template should not be selected</xsl:text>
  </xsl:template>

  <xsl:template match="div[@type='file']" mode="startTemplating">

    <xsl:call-template  name="message"><xsl:with-param name="bias" select="'3'"/>
    <xsl:with-param name="theMessage">fileArray: templating "<xsl:value-of select="@id"  />"; </xsl:with-param></xsl:call-template>
    
    <div v="{system-property('xsl:version')}">
      <xsl:copy-of select="@*" />
      <xsl:apply-templates select="common:node-set($htmlTemplate)" mode="templating">
	<xsl:with-param name="fileId" select="@id"/>
	<xsl:with-param name="nodeID" select="@nodeId"/>
      </xsl:apply-templates>
    </div>
  </xsl:template>

  <!-- don't proceed in templating mode; node is called only in mode startTemplating -->
  <xsl:template match="div[@type='file']" mode="templating" />
  
  <xsl:template match="/ | node() | @* | comment()" mode="templating">
    <xsl:param name="fileId" />
    <xsl:param name="nodeId" />
    <xsl:copy>
      <xsl:apply-templates select="node() | @* | comment()" mode="templating">
	<xsl:with-param name="fileId" select="$fileId"/>
	<xsl:with-param name="nodeID" select="@nodeId"/>
      </xsl:apply-templates>
    </xsl:copy>
    
  </xsl:template>

  



  <xsl:template match="a[@siteHref]" mode="templating">
    <xsl:param name="fileId" />
    <xsl:param name="nodeId" />
    <xsl:copy>
      <xsl:copy-of select="@*" />
      <!-- overwrite href attribute with cross site href attribute -->
      <xsl:attribute name="href">
	<xsl:value-of select="@siteHref" />
      </xsl:attribute>
      <xsl:apply-templates select="node() | comment()" mode="templating">
	<xsl:with-param name="fileId" select="$fileId"/>
	<xsl:with-param name="nodeID" select="@nodeId"/>
      </xsl:apply-templates>
    </xsl:copy>
  </xsl:template>



  
  <!-- macros -->
  <xsl:template match="*[@semAuth]" mode="templating">
    <xsl:param name="fileId" />
    <xsl:param name="nodeId" />

    <xsl:copy>
      <xsl:copy-of select="@*" />
      <xsl:choose>

	
	<!--
	    OK  <title  semAuth="pageTitle">Seitentitel</title>
	    <li semAuth="browse_li">browse</li>   back | up | next | 
	    OK  <div semAuth="pageContent">
	    OK  <div semAuth="breadcrumb">bread > crumb</div>
	    OK  <div semAuth="vNav" > Alternative vNavFull
	-->

	<xsl:when test="@semAuth = 'pageTitle' ">
	  <xsl:variable name="title">
	    <xsl:value-of select="common:node-set($pass1000)//*[@id=$fileId]/@title"/>
	  </xsl:variable>
	  <xsl:choose>
	    <xsl:when test="$title != ''">
	      <xsl:value-of select="$title"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="@fileId"/>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:when>

	<xsl:when test="@semAuth = 'shortTitle' ">
	  <xsl:variable name="title">
	    <xsl:value-of select="common:node-set($pass1000)//*[@id=$fileId]/@shortTitle"/>
	  </xsl:variable>
	  <xsl:choose>
	    <xsl:when test="$title != ''">
	      <xsl:value-of select="$title"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="@fileId"/>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:when>

	<xsl:when test="@semAuth = 'pageContent' ">
	  <xsl:apply-templates select="common:node-set($pass1000)//*[@id=$fileId]/node()" mode="templating">
	    <xsl:with-param name="fileId" select="$fileId"/>
	    <xsl:with-param name="nodeID" select="@nodeId"/>
	  </xsl:apply-templates>
	</xsl:when>

	
	<xsl:when test="@semAuth = 'build' ">
	  <xsl:value-of select="$build"/>
	</xsl:when>

	<xsl:when test="@semAuth = 'vendor' ">
	  <xsl:value-of select="system-property('xsl:version')" /> by <xsl:value-of select="system-property('xsl:vendor')" /> (<xsl:value-of select="system-property('xsl:vendor-url')" />)
	</xsl:when>

	<xsl:when test="@semAuth = 'TOC' ">
	  <xsl:apply-templates select="common:node-set($TOC)//div[@type='file'][@id = $fileId]" mode="TOC">
	    <xsl:with-param name="depth">
	      <xsl:choose>
		<xsl:when test="@semAuthParam">
		  <xsl:value-of select="@semAuthParam" />
		</xsl:when>
		<xsl:otherwise>9</xsl:otherwise>
	      </xsl:choose>
	    </xsl:with-param>
	  </xsl:apply-templates>
 	</xsl:when>

	<xsl:when test="@semAuth = 'vNav' ">
	  <xsl:apply-templates select="common:node-set($TOC)//div[@type='file'][@id = $fileId]" mode="vNav" />
	</xsl:when>

	<xsl:when test="@semAuth = 'breadcrumb' ">
	  <xsl:apply-templates select="common:node-set($TOC)//div[@type='file'][@id = $fileId]/ancestor::div[@type='file'][1]" mode="breadcrumb" />
	</xsl:when>

	<xsl:when test="@semAuth = 'browse_li' ">
	  <xsl:apply-templates select="common:node-set($TOC)//div[@type='file'][@id = $fileId]" mode="browse_li" />
	</xsl:when>

	
	<xsl:when test="@semAuth = 'languages' ">
	  <xsl:variable name="multiLanguageId">
	    <xsl:value-of select="common:node-set($pass1000)//*[@id=$fileId]/@multiLanguageId"/>
	  </xsl:variable>

	  <xsl:for-each select="common:node-set($nf2)//node[@multiLanguageId = $multiLanguageId]">
	    <li>
	      
	      <xsl:variable name="mmIdRef" select="@mmIdRef" />
	      <xsl:value-of select="$mmIdRef" />
	      
	      <a href="{@id}.html" title="{common:node-set($nf2)//node[@mmId=$mmIdRef]/@TEXT}">
		<!-- <xsl:value-of select="substring(@languageTag,2,2)" /> -->
		<xsl:value-of select="@languageTag" />
	      </a>
	      <!-- 
		   <ul>
		   <xsl:for-each select="common:node-set($nf2)//node[@mmId=$mmIdRef]/@*">
		   <li>
		   <xsl:value-of select="name()" />=<xsl:value-of select="." />
		   </li>
		   </xsl:for-each>
		   </ul>
	      -->
	      <!--
		  <xsl:if test="position() != last()">
		  <xsl:text> | </xsl:text>
		  </xsl:if>
	      -->
	    </li>
	  </xsl:for-each>
	</xsl:when>
	
	<xsl:otherwise>
	  <xsl:call-template  name="message"><xsl:with-param name="bias" select="'0'"/>
          <xsl:with-param name="theMessage">Warning 2250: skipping unknown macro <xsl:value-of select="@semAuth" /></xsl:with-param></xsl:call-template>
	</xsl:otherwise>
	
      </xsl:choose>
    </xsl:copy>
    
  </xsl:template>


  
  <!-- TOC -->

  <xsl:template match="*" mode="TOC">
    <xsl:message>ERROR 150: template should not be selected: match="*" mode="TOC" </xsl:message>
  </xsl:template>
  
  <xsl:template match="div[@type='file'] | section" mode="TOC">
    <xsl:param name="depth" />
    <xsl:choose>
      <xsl:when test="$depth > 0 and ./section">
	<h4>Inhalt</h4>
	<xsl:apply-templates select="." mode="TOC_ul">
	  <xsl:with-param name="depth" select="$depth" />
	</xsl:apply-templates>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="div[@type='file'] | section" mode="TOC_ul">
    <xsl:param name="depth" />
    <xsl:choose>
      <xsl:when test="$depth > 0 and ./section">
	<ul  class="vlist" type="TOC">
	  <xsl:for-each select="section">
	    <li>
	      <a href="#{@id}"><xsl:value-of select="@TEXT" /></a>
	      <xsl:apply-templates select="." mode="TOC_ul">
		<xsl:with-param name="depth" select="$depth - 1" />
	      </xsl:apply-templates>
	    </li>
	  </xsl:for-each>
	</ul>
      </xsl:when>
    </xsl:choose>
  </xsl:template>


  <!-- vNav -->

  <xsl:template match="*" mode="vNav">
    <xsl:message>ERROR 151: template should not be selected: match="*" mode="vNav" </xsl:message>
  </xsl:template>
  
  <xsl:template match="div[@type='file']" mode="vNav">
    <xsl:variable name="myGenId">
      <xsl:value-of select="generate-id()" />
    </xsl:variable>

    <ul class="vlist"> 
      
      <!-- parent: it is the breadcrumb which also shows the parent file: nothing to do here  -->

      <!-- preceding siblings -->
      <xsl:for-each select="preceding-sibling::div[@type='file']">
	<li class="vNavSibling" >
	  <xsl:call-template name="vNavDisplayItem" />
	</li>
      </xsl:for-each>
      
      <!-- me: strong -->
      <li class="active selected">
	<xsl:call-template name="vNavDisplayItem" />

	<!-- childs -->
	<xsl:if test="descendant::div[@type='file']">
	  <ul class="vlist"> 
	    <xsl:for-each select="descendant::div[@type='file'][generate-id(ancestor::div[@type='file'][1]) = $myGenId]">
	      <li class="vNavChild">
		<xsl:call-template name="vNavDisplayItem" />
	      </li>
	    </xsl:for-each>
	  </ul>
	</xsl:if>
      </li>
      
      <!-- following siblings -->
      <xsl:for-each select="following-sibling::div[@type='file']">
	<li class="vNavSibling">
	  <xsl:call-template name="vNavDisplayItem" />
	</li>
      </xsl:for-each>

    </ul>
  </xsl:template>


  <!-- display entry in vNav -->
  <xsl:template name="vNavDisplayItem">
    <a href="{@id}.html">
      <xsl:choose>
	<xsl:when test="@shortTitle">
	  <xsl:value-of select="@shortTitle" />
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="@title" />
	</xsl:otherwise>
      </xsl:choose>
    </a>
  </xsl:template>


  <!-- breadcrumb -->

  <xsl:template match="*" mode="breadcrumb">
    <xsl:message>ERROR 151: template should not be selected: match="*" mode="breadcrumb" </xsl:message>
  </xsl:template>

  <xsl:template match="div[@type='file']" mode="breadcrumb">
    <xsl:variable name="myGenId">
      <xsl:value-of select="generate-id()" />
    </xsl:variable>

    <xsl:if test="ancestor::div[@type='file']">
      <xsl:apply-templates select="ancestor::div[@type='file'][1]" mode="breadcrumb" />
      <xsl:text> » </xsl:text>
    </xsl:if>
    <xsl:call-template name="vNavDisplayItem" />

  </xsl:template>



  <!-- browse_li -->

  <xsl:template match="*" mode="browse_li">
    <xsl:message>ERROR 151: template should not be selected: match="*" mode="browse_li" </xsl:message>
  </xsl:template>

  <xsl:template match="div[@type='file']" mode="browse_li">

    <xsl:if test="preceding::div[@type='file'] or ancestor::div[@type='file']">
      <li><a href="{(preceding::div[@type='file'] | ancestor::div[@type='file'])[1]/@id}.html">prev</a></li>
    </xsl:if>

    <xsl:if test="ancestor::div[@type='file']">
      <li><a href="{ancestor::div[@type='file'][1]/@id}.html">up</a></li>
    </xsl:if>

    <xsl:if test="following::div[@type='file'] or descendant::div[@type='file']">
      <li><a href="{(following::div[@type='file'] | descendant::div[@type='file'])[1]/@id}.html">next</a></li>
    </xsl:if>


  </xsl:template>



  
  


  <!-- $$$$$
       serialize final output
       $$$$$ -->
  
  <xsl:template match="/ | * | @* | text()" mode="final">
    <xsl:copy>
      <xsl:apply-templates select="* | @* | text()" mode="final" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="quiz" mode="final" />


  
  <xsl:template match="div[@type='file']" mode="final">
    <xsl:choose>

      <xsl:when test="position() > 1 and $multipleFiles = 'yes' ">
	<xsl:message>sorry, semAuthLite can't output multiple files: supressing [[ <xsl:value-of select="@id" /> | <xsl:value-of select="@title" /> ]]</xsl:message>
      </xsl:when>

      <xsl:otherwise>
	<xsl:copy-of select="node()" />
      </xsl:otherwise>
      
      <!-- <vendor><xsl:value-of select="system-property('xsl:version')" /> by <xsl:value-of select="system-property('xsl:vendor')" /> (<xsl:value-of select="system-property('xsl:vendor-url')" />)</vendor> -->
    </xsl:choose>

  </xsl:template>


  

  
  <!-- debug and other templates -->
  
  <xsl:template name="message">
    <xsl:param name="bias" />
    <xsl:param name="theMessage"/>
    <xsl:choose>
      <xsl:when test="$verbose &gt;= $bias">
	<xsl:message><xsl:value-of select="$bias" />: <xsl:value-of select="$theMessage" /></xsl:message>
      </xsl:when>
      <xsl:otherwise>
	<!-- debug the debug message system <xsl:message>SUPRESSING message <xsl:value-of select="$theMessage" /></xsl:message>-->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


</xsl:stylesheet>
