package plugins.semauth;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.net.*;
import javax.swing.*;
import freemind.modes.ModeController;

public class SemAuthDebugView extends JFrame implements ActionListener
{
	private static final long serialVersionUID = 1L;
	private String index;
	private ModeController controller;
	private SemAuthBrowser browser;
	private JButton reload, open, close;
	private JLabel info;
	
	public SemAuthDebugView(ModeController pModeController)
	{		
		// frame configuration
		this.controller = pModeController;
		this.controller.getFrame().setWaitingCursor(false);
		this.tempDeployment();
		this.setTitle(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("APP_TITLE") + " " + 
				SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("APP_TITLE_DEBUG"));
		this.setLayout(new GridBagLayout());		
		
		// initialize components
		this.browser = new SemAuthBrowser(this.index);
		this.setMinimumSize(new Dimension(1024, 800));
		this.browser.setSize(this.getSize());
		this.reload = new JButton(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("BUTTON_REFRESH"));
		this.reload.addActionListener(this);
		this.open = new JButton(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("BUTTON_OPEN_BROWSER"));
		this.open.addActionListener(this);
		this.close = new JButton(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("BUTTON_CLOSE"));
		this.close.addActionListener(this);
		this.info = new JLabel(SemAuthProperties.TEXT.get(SemAuthLib.getUserLanguage()).get("MESSAGE_WAIT"));
		this.info.setVisible(false);
		this.info.setForeground(Color.red);
		this.info.setFont(new Font("Serif", Font.BOLD, 48));

		// add components to frame
		this.add(this.reload, SemAuthLib.makeLayout(1, 1, 1, 1));		
		this.add(this.open, SemAuthLib.makeLayout(2, 1, 1, 1));		
		this.add(this.close, SemAuthLib.makeLayout(3, 1, 1, 1));		
		this.add(this.info, SemAuthLib.makeLayout(4, 1, 1, 1));		
		this.add(this.browser, SemAuthLib.makeLayout(1, 2, 4, 5));		
		
		// prepare frame and show it
		this.pack();
		this.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		if(arg0.getSource().equals(this.close))
		{
			this.setVisible(false);
		}
		else if(arg0.getSource().equals(this.reload))
		{
			this.info.setVisible(true);
			this.invalidate();
			this.validate();
			this.repaint();
			new SemAuthDebugView(this.controller);
			this.setVisible(false);
		}
		else if(arg0.getSource().equals(this.open))
		{
			try 
			{
				SemAuthLib.openWebpage(new URI(this.index));
			} 
			catch (URISyntaxException e) 
			{
				SemAuthLib.debug(e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	private void tempDeployment()
	{
		String name = this.controller.getMap().getRootNode().getText().replaceAll(" ", "");
		File output = new File(SemAuthProperties.PLUGIN_WWW + name); 

		if(SemAuthLib.localDeployment(this.controller))
		{
			try 
			{
				this.index = "file://" + 							
						output.getAbsolutePath().replace("\\", "/") + 
						"/" + name +
						".html";		
				
				System.out.println(this.index);
					
			} 
			catch (Exception e)
			{
				SemAuthLib.debug(e.getMessage());
				e.printStackTrace();
			}	
		}
		else
		{
			SemAuthLib.debug("could not execute local deployment");
		}		
	}
}
