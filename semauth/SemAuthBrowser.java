package plugins.semauth;

import java.net.URLEncoder;
import javafx.application.Platform;
import javafx.embed.swing.*;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.web.*;

public class SemAuthBrowser extends JFXPanel
{
    private static final long serialVersionUID = 1L;
    private AnchorPane anchorPane;
    private WebView webBrowser;
    private Scene scene;
    
    public SemAuthBrowser(java.lang.String url)
    {
        Platform.runLater(
        		new Runnable()
        		{
		            @SuppressWarnings("deprecation")
					@Override
		            public void run()
		            {
		            	try
		            	{
		            		anchorPane = new AnchorPane();
			                webBrowser = new WebView();
			                anchorPane.setMinSize(900, 700);			                
			                webBrowser.setMinSize(900, 700);			
			                anchorPane.getChildren().add(webBrowser);
			                
			                scene = new Scene(anchorPane);                
			                webBrowser.getEngine().load(URLEncoder.encode(url).replace("%3A", ":"));
			                setScene(scene);
		            	}
		            	catch(Exception ex)
		            	{
		            		SemAuthLib.debug(ex.getMessage());
		            		ex.printStackTrace();
		            	}
		            }
		            
		          
		       });
    }
    
    @SuppressWarnings("deprecation")
    public boolean redirect(java.lang.String pUrl)
    {
    	if(pUrl == null || pUrl.equals(""))
    	{
    		SemAuthLib.debug("redirect method got invalid URL");
    		return false;
    	}
    	
    	webBrowser.getEngine().load(URLEncoder.encode(pUrl).replace("%3A", ":"));
    	return true;
    }
}