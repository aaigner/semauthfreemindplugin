package plugins.semauth;

import freemind.extensions.ExportHook;

public class SemAuthExportController extends ExportHook
{
	public void startupMapHook() 
	{		
		super.startupMapHook();
 		new SemAuthExportView(this.getController());		
	}
}
