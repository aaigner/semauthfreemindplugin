package plugins.semauth;

import java.awt.*;
import java.io.*;
import java.net.URI;
import java.text.*;
import java.util.*;
import java.util.zip.*;
import freemind.modes.ModeController;
import sun.net.ftp.impl.FtpClient;

public class SemAuthLib 
{	
	public static String getUserLanguage()
	{
		if(Locale.getDefault().toString().equals(SemAuthProperties.DE_de))
		{
			return SemAuthProperties.DE_de;
		}
		
		return SemAuthProperties.US_en;
	}
	
	public static boolean localDeployment(ModeController pModeController)
	{
		if(pModeController == null)
		{
			SemAuthLib.debug("null pointer or invalid output path detected before local deployment");
			return false;
		}
				
		try 
		{
			String name = pModeController.getMap().getRootNode().getText().replaceAll(" ", "");
			File mindmap = new File(SemAuthProperties.PLUGIN_TEMP + name + ".mm");			
			File output = new File(SemAuthProperties.PLUGIN_WWW + name); 
			
			String cmd = "java -classpath " 
					+ SemAuthProperties.PLUGIN_WWW + "xslt" + File.separator + "saxon9he.jar net.sf.saxon.Transform " 
					+ "" + mindmap.getAbsolutePath() + " " 
					+ "" + SemAuthProperties.PLUGIN_WWW + "xslt" + File.separator + "semAuth2.xsl " 
					+ "t=../www/templates/MaxMustermann.html "
					+ "p=" + output + File.separator;
			
			String[] cmdV2 = new String[]
					{
						"java",
						"-classpath",
						SemAuthProperties.PLUGIN_WWW + "xslt" + File.separator + "saxon9he.jar",
						"net.sf.saxon.Transform", 
						mindmap.getAbsolutePath(),
						SemAuthProperties.PLUGIN_WWW + "xslt" + File.separator + "semAuth2.xsl", 
						"t=../www/templates/MaxMustermann.html",
						"p=" + output + File.separator
					};
			
			if(SemAuthProperties.DEBUG)
			{
				System.out.println(cmd);
			}
			
			if(mindmap.exists())
			{
				mindmap.delete();
			}
			
			if(output.exists())
			{
				SemAuthLib.deleteDirectory(output);				
			}
			
			output.mkdir();			
			pModeController.save(mindmap);	
			Process process = Runtime.getRuntime().exec(cmdV2);				
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));			
			SemAuthLib.debug(reader.readLine());
			reader.close();
			Thread.sleep(2000);						
			mindmap.delete();
						
			for(File html : output.listFiles())
			{
				//html.renameTo(new File(html.getPath() + File.separator + html.getName().replaceAll("\\W+", "")));
				html.renameTo(new File(html.getParent() + File.separator + html.getName().replaceAll(" ", "")));
			}
			
			
			SemAuthLib.debug("local deployment successfully done");
			return true;
		} 
		catch (Exception e) 
		{
			SemAuthLib.debug(e.getMessage());
			e.printStackTrace();
			return false;
		}		
	}
	
	public static boolean ftpDeployment(ModeController pModeController, String pServer, String pUsername, String pPassword, String pDirectory)
	{		
		try
		{
			String name = pModeController.getMap().getRootNode().getText().replaceAll(" ", "");
			File output = new File(SemAuthProperties.PLUGIN_WWW + name); 
			
			if(!SemAuthLib.localDeployment(pModeController))
			{
				return false;
			}
			
			pDirectory += "/" + name;			
			SemAuthLib.upload(output, output, pServer, pUsername, pPassword, pDirectory);			
			return true;
		}
		catch(Exception e)
		{
			SemAuthLib.debug(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	
	private static boolean upload(File pBase, File pFile, String pServer, String pUsername, String pPassword, String pDirectory)
	{	
		for(String file : pFile.list())
		{
			File tmp = new File(pFile.getAbsolutePath() + File.separator + file);
			
			if(!tmp.isFile())
			{
				upload(pBase, tmp, pServer, pUsername, pPassword, pDirectory);
			}
			else
			{
				try
				{
					String dir = pDirectory + "/" + pFile.getAbsolutePath().replace(pBase.getAbsolutePath(), "").replace(File.separatorChar, '/');					
					FileInputStream inputStream = new FileInputStream(pFile.getAbsolutePath() + File.separator + file);					
					FtpClient fc = (FtpClient) FtpClient.create(pServer);
					fc.login(pUsername, pPassword.toCharArray());
					fc.setBinaryType();
					
					String[] folders = dir.split("/");
					String current = "";
					for(String s : folders)
					{
						if(s == null || s.isEmpty())
							continue;
						
						current += s + "/";
						
						try					
						{
							fc.makeDirectory(current);
						}
						catch(Exception ex)
						{
							SemAuthLib.debug(ex.getMessage());
							ex.printStackTrace();
						}
					}

					fc.putFile(dir + "/" + file, inputStream);					
					//fc.close();
					inputStream.close();
				}
				catch(Exception e)
				{
					SemAuthLib.debug(e.getMessage());
					e.printStackTrace();
					return false;
				}
			}
		}
		
		return true;
	}
		
	public static GridBagConstraints makeLayout(int x, int y, int w, int h)
	{
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridheight = h;
		gbc.gridwidth = w;
		gbc.gridx = x;
		gbc.gridy = y;		
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(5, 5, 5, 5);
		
		return gbc;
	}
	
	public static void debug(String pMessage)
	{
		if(SemAuthProperties.DEBUG)
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println("[semauth|"+dateFormat.format(date)+"] " + pMessage);
		}
	}
	
	public static void openWebpage(URI uri) 
 	{
 	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
 	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) 
 	    {
 	        try 
 	        {
 	            desktop.browse(uri);
 	        } 
 	        catch (Exception e) 
 	        {
 	        	SemAuthLib.debug(e.getMessage());
 	            e.printStackTrace();
 	        }
 	    }
 	}
	
	public static boolean deleteDirectory(File pInput)
	{
		if(pInput.isFile())
		{
			pInput.setWritable(true);
			boolean code = pInput.delete();
			return code;
		}
		
		for(String p : pInput.list())
		{
			deleteDirectory(new File(pInput.getAbsolutePath() + File.separator + p));
		}
		
		return true;
	}

	@SuppressWarnings("resource")
	private static void addFileToZip(String path, String srcFile, ZipOutputStream zip)
	{
		try
		{
		    File folder = new File(srcFile);
		    
		    if (folder.isDirectory()) 
		    {
		      addFolderToZip(path, srcFile, zip);
		    } 
		    else 
		    {
		    	byte[] buf = new byte[SemAuthProperties.BUFFER_SIZE];
		    	int len;
		    	FileInputStream in = new FileInputStream(srcFile);
		    	zip.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
		      
		    	while ((len = in.read(buf)) > 0) 
		    	{
		    		zip.write(buf, 0, len);
		    	}
		    }
		}
		catch(Exception e)
		{
			SemAuthLib.debug(e.getMessage());
			e.printStackTrace();
		}
	}

	private static void addFolderToZip(String path, String srcFolder, ZipOutputStream zip)
	{
		try
		{
			File folder = new File(srcFolder);

			for (String fileName : folder.list()) 
			{
				if (path.equals("")) 
				{
					addFileToZip(folder.getName(), srcFolder + "/" + fileName, zip);
				} 
				else 
				{
					addFileToZip(path + "/" + folder.getName(), srcFolder + "/" + fileName, zip);
				}
			}
		}
		catch(Exception e)
		{
			SemAuthLib.debug(e.getMessage());
			e.printStackTrace();
		}
	}
}
