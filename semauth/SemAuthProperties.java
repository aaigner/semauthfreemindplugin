package plugins.semauth;

import java.io.File;
import java.util.HashMap;

public class SemAuthProperties 
{
	public static final boolean DEBUG = true;
	public static final int BUFFER_SIZE = 4096;
	public static final String FTP_URL = "ftp://%s:%s@%s/%s;type=i";
	public static final String FTP_USER = "";
	public static final String FTP_PASS = "";
	public static final String FTP_HOST = "";
	public static final String FTP_DIR = "";
	public static final String VERSION = "1.0";
	public static final String DE_de = "de_DE";
	public static final String US_en = "en_US";
	public static final String HOMEPAGE = "http://fhd.semauth.de";
	public static final String MANUAL = "plugin" + File.separator + "semauth" + File.separator + "manual.pdf";		
	public static final String PLUGIN_HOME = "plugins" + File.separator + "semauth" + File.separator;;
	public static final String PLUGIN_TEMP = PLUGIN_HOME.replace(" ", "%20") + "temp" + File.separator;
	public static final String PLUGIN_WWW = PLUGIN_HOME + "www" + File.separator;
	public static HashMap<String, HashMap<String, String>> TEXT = new HashMap<String, HashMap<String, String>>()
	{
		private static final long serialVersionUID = 1L;
		{
			this.put(SemAuthProperties.DE_de, new HashMap<String, String>()
			{
				private static final long serialVersionUID = 1L;
				{	
					this.put("APP_TITLE", "SemAuth");
					this.put("APP_VERSION", "Version");
					this.put("APP_TITLE_EXPORT", "Exportieren");
					this.put("APP_TITLE_HELP", "Hilfe");
					this.put("APP_TITLE_DEBUG", "Vorschau");
					this.put("BUTTON_CLOSE", "schließen");
					this.put("BUTTON_START_EXPORT", "exportieren");
					this.put("BUTTON_CHOOSE_DIR", "Zielordner wahlen");
					this.put("BUTTON_REFRESH", "Inhalt neu laden");
					this.put("BUTTON_OPEN_MANUAL", "Anleitung öffnen (PDF");
					this.put("BUTTON_OPEN_HOMEPAGE", "Webseite besuchen (URL)");
					this.put("BUTTON_OPEN_BROWSER", "im Browser öffnen");
					this.put("ERROR_BROWSER_NOT_FOUND", "Ihr Browser konnte nicht geöffnet werden. Bitte besuchen Sie ");
					this.put("ERROR_MANUAL_NOT_FOUND", "Anleitung konnte nicht geöffnet werden. Bitte besuchen Sie ");		
					this.put("ERROR_EMPTY_FTP", "Bitte zunächst alle Einstellungen eingeben");
					this.put("ERROR_INTERNAL", "leider ist ein interner Fehler aufgetreten");
					this.put("MESSAGE_WAIT", "bitte warten ...");
					this.put("MESSAGE_MAKE_ZIP", "ZIP Datei erstellen");
					this.put("MESSAGE_LOCAL_DEPLOYMENT", "Lokaler Export");
					this.put("MESSAGE_FTP_DEPLOYMENT", "FTP Upload");
					this.put("MESSAGE_CHOOSE_TYPE", "Bitte Methode wählen");
					this.put("MESSAGE_CHOOSEN_DIR", "gewählter Ausgabeort");
					this.put("MESSAGE_FTP_HOST", "FTP Server");
					this.put("MESSAGE_FTP_USER", "FTP Nutzer");
					this.put("MESSAGE_FTP_PASS", "FTP Passwort");
					this.put("MESSAGE_FTP_DIR", "Server Verzeichnis");
					this.put("MESSAGE_EXPORT_OK", "Export erfolgreich durchgeführt");
				}
			});
				
			this.put(SemAuthProperties.US_en, new HashMap<String, String>()
			{
				private static final long serialVersionUID = 1L;
				{	
					this.put("APP_TITLE", "SemAuth");
					this.put("APP_VERSION", "Version");
					this.put("APP_TITLE_EXPORT", "Export");
					this.put("APP_TITLE_HELP", "Help");
					this.put("APP_TITLE_DEBUG", "Preview");
					this.put("BUTTON_CLOSE", "close");
					this.put("BUTTON_START_EXPORT", "start export");
					this.put("BUTTON_CHOOSE_DIR", "choose output folder");
					this.put("BUTTON_REFRESH", "refresh content");
					this.put("BUTTON_OPEN_MANUAL", "open Local Manual (PDF)");
					this.put("BUTTON_OPEN_HOMEPAGE", "visit Homepage (URL)");
					this.put("BUTTON_OPEN_BROWSER", "open in browser (default)");
					this.put("ERROR_BROWSER_NOT_FOUND", "sorry, could not open browser. Please visit ");
					this.put("ERROR_MANUAL_NOT_FOUND", "sorry, could not open manual. Please look at ");
					this.put("ERROR_EMPTY", "please enter the parameter first");
					this.put("ERROR_INTERNAL", "unfortunately an internal error occured");
					this.put("MESSAGE_WAIT", "PLEASE WAIT ...");
					this.put("MESSAGE_MAKE_ZIP", "make ZIP file");
					this.put("MESSAGE_LOCAL_DEPLOYMENT", "local deployment");
					this.put("MESSAGE_FTP_DEPLOYMENT", "FTP deployment");
					this.put("MESSAGE_CHOOSE_TYPE", "please Select Mode first");
					this.put("MESSAGE_CHOOSEN_DIR", "selected output folder");
					this.put("MESSAGE_FTP_HOST", "FTP host");
					this.put("MESSAGE_FTP_USER", "FTP username");
					this.put("MESSAGE_FTP_PASS", "FTP password");
					this.put("MESSAGE_FTP_DIR", "upload directory");
					this.put("MESSAGE_EXPORT_OK", "export successfully done");
				}
			});
		}
	};
}
